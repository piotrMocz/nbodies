//#include "read-and-test.h"
#include "krawczyk-newton.h"
#include "test-and-help-functions.h"
#include "solutions-tests.h"
#include "find-symmetry.h"

//extern Count globalZeros;

/*MyInterval intervalHull(MyInterval A, MyInterval B)
{
    MyDouble leftPoint = (A.leftBound() <= B.leftBound()) ? A.leftBound() : B.leftBound();
    MyDouble rightPoint = (A.rightBound() >= B.rightBound()) ? A.rightBound() : B.rightBound();

    return MyInterval(leftPoint, rightPoint);
}
*/
void intervalHullBodies(Bodies &sol1, Bodies &sol2, Bodies &unionBodies)
{
    int N = sol1.N;

    for (int i = 0; i < N - 1; i++)
    {
        unionBodies.setX(i, intervalHull(sol1.x(i), sol2.x(i)));
        unionBodies.setY(i, intervalHull(sol1.y(i), sol2.y(i)));
    }
}

bool theSameSolution(Bodies &sol1, Bodies &sol2, int permutacje[])
{
    int N = sol1.N;
    //ofstream resFile("res-loc.csv");

    Bodies permSol(sol2); // tutaj bedzie rozwiazanie sol2 przepermutowane zgodnie z permutacje[]
    for (int i = 0; i < N; i++)
    {
        MyInterval x1 = sol1.x(i), x2 = sol2.x(permutacje[i]);
        if (emptyIntersection(x1, x2))
            return false; // different solutions
        if (i < N - 1)
            permSol.setX(i, x2);

        MyInterval y1 = sol1.y(i), y2 = sol2.y(permutacje[i]);
        if (emptyIntersection(y1, y2))
            return false; // different solutions
        if (i < N - 1)
            permSol.setY(i, y2);
    }
    // przeciecie nie jest puste

    Bodies unionBodies(sol1); // dzieki temu mamy wlasciwe masy cial!!!

    intervalHullBodies(sol1, permSol, unionBodies);

    Count cZeros = search(unionBodies);
    if (cZeros.zeros == 1)
        return true; // sol1 and sol2 are the same solution
    else
    {
        cZeros = blowUp(unionBodies);
        return (cZeros.zeros == 1);
    }

    //resFile.close();
}

int readBodiesFromBinaryFile(const char *nazwaPliku, vector<Bodies> &ccSolutions)
{
    int numberOfCC = 0;
    //int sizeOfBodies = sizeof(Bodies);

    Bodies testBodies(NUM_BODIES);

    ifstream infile;
    infile.open(nazwaPliku, ios::binary | ios::in);
    if (infile.is_open())
    {

        while (testBodies.readBinaryData(infile))
        {
            ccSolutions.push_back(testBodies);
            //testBodies.printData();
            numberOfCC++;
        }
        infile.close();
    }
    else
        cout << "BLAD OTWARCIA PLIKU " << nazwaPliku << endl;
    return numberOfCC;
}

bool nextPermutationToCheck(int perm[])
{
    bool found;
    int p[NUM_BODIES - 1];

    for (int i = 0; i < NUM_BODIES - 2; i++)
        p[i] = perm[i];
    p[NUM_BODIES - 2] = perm[NUM_BODIES - 1];
    found = nextPerm(p, NUM_BODIES - 1);
    for (int i = 0; i < NUM_BODIES - 2; i++)
        perm[i] = p[i];
    perm[NUM_BODIES - 1] = p[NUM_BODIES - 2];

    return found;
}
void testujZera(const char *nazwaPliku, int &ccNo, vector<Bodies> &ccSolutions,
                vector<int> &markedSolutions, int &uniqueSol)
// ccNo -- liczba wczytanych rozwiazan (rozmiar tablicy ccSolutions i markedSolutions)
// uniqueSol -- liczba unikalnych rozwiazan
{
    Bodies sol1(NUM_BODIES);
    Bodies sol2(NUM_BODIES);
    //vector<Bodies> ccSolutions;
    uniqueSol = 0;

    //ccNo = readBodiesFromBinaryFile(nazwaPliku, ccSolutions);
    //cout << endl;
    //cout << "Wczytana liczba cc = ";
    //cout << ccNo << endl;

    // Create a vector of size ccNo with
    // all values as 0.
    //vector<int> markedSolutions(ccNo, 0); // tylko pozycje oznaczone 0 sa unikalnymi zerami (maja szane byc)
    for (int i = 0; i < ccNo; i++)
        markedSolutions.push_back(0);

    //*/
    for (int i = 0; i < ccNo; i++)
    {
        sol1 = ccSolutions[i];
        /*
        cout << "wczytane dane " << endl;
        sol1.printData();
        cout << "--------------------------" << endl;
        cout << endl;
*/
        for (int j = i + 1; j < ccNo; j++)
        {
            if (markedSolutions[j] == 1)
                continue;

            // inicjuje permutacje na identycznosc
            int permutacje[NUM_BODIES];
            for (int k = 0; k < NUM_BODIES; k++)
                permutacje[k] = k;

            sol2 = ccSolutions[j];
            for (;;)
            {
                if (theSameSolution(sol1, sol2, permutacje))
                {
                    //cout << "rozwiazania " << i << " i " << j << "sa takie same!" << endl;
                    markedSolutions[j] = 1;
                    break;
                } // takie same rozwiazania
                if (!equalMasses)
                    break; // permutacje maja sens tylko dla rownych mas

                if (!nextPermutationToCheck(permutacje))
                    break; // nie ma kolejnej permutacji
            }
        }
    }

    for (int i = 0; i < ccNo; i++)
        if (markedSolutions[i] == 0)
        {
            if (fixedOrderOfBodies && !distanceTest(ccSolutions[i]))
            {
                markedSolutions[i] = 1;
                //cout << "distanceTest() nie przeszedl!" << endl;
            }
            else
                uniqueSol++;
        }
}

void testujSymetrie(int ccNo, vector<Bodies> &ccSolutions, vector<int> &markedSolutions)
{
    int symOXCounter = 0;
    int symColCounter = 0;
    int symLineCounter = 0;

    bool symBodies[ccNo];
    for (int i = 0; i < ccNo; i++)
        symBodies[i] = false;

    cout << endl;
    cout << "Different CC:" << endl;
    int realCC = 0;
    for (int i = 0; i < ccNo; i++)
        if (markedSolutions[i] == 0)
        {
            realCC++;
            cout << "---------------------" << endl;
            cout << "position " << i << endl;
            ccSolutions[i].printData(true); // writes distances from 0
            cout << flush;

            writePotAndInertia(ccSolutions[i]);

            if (colinearCC(ccSolutions[i]))
            {
                symColCounter++;
                cout << "collinear solution no " << symColCounter << endl;
            }
            if (equalMasses)
            {
                if (symmetricOXCC(ccSolutions[i]))
                {
                    //symBodies[i] = true;
                    symOXCounter++;
                    cout << "symmetric with respect to OX no " << symOXCounter << endl;
                }
                if (!symBodies[i]) // rozwiąznie nie jest symetryczne wzgledem osi OX
                    if (findSymByLine(ccSolutions[i]))
                    {
                        symBodies[i] = true;
                        symLineCounter++;
                        cout << "reflectional symmetry with respect to other line " << symLineCounter << endl;
                    }
            }
            /*
            if (!distanceTest(ccSolutions[i]))
                cout << "distanceTest() nie przeszedl!" << endl;
            cout << endl;
            */
        }
    cout << endl;
    cout << "Number of different cc = " << realCC << endl;
}

void lookInsideOfInterData()
{
    Bodies sol(NUM_BODIES);
    vector<Bodies> ccData;

    int ccNo = readBodiesFromBinaryFile(plikInterData, ccData);
    cout << "Wczytana liczba cc = ";
    cout << ccNo << endl;

    for (int i = 0; i < ccNo; i++)
    {
        sol = ccData[i];
        sol.printData();
        cout << endl;

        sol.printDiam();
    }
}
