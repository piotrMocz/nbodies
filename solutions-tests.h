//MyInterval intervalHull(MyInterval A, MyInterval B);
void intervalHullBodies(Bodies &sol1, Bodies &sol2, Bodies &unionBodies);
bool theSameSolution(Bodies &sol1, Bodies &sol2, int permutacje[]);
int readBodiesFromBinaryFile(const char *nazwaPliku, vector<Bodies> &ccSolutions);
void testujZera(const char *nazwaPliku, int &ccNo, vector<Bodies> &ccSolutions,
                vector<int> &markedSolutions, int &uniqueSol);
void testujSymetrie(int ccNo, vector<Bodies> &ccSolutions, vector<int> &markedSolutions);

void lookInsideOfInterData();