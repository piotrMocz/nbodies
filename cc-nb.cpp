//#define _USE_MATH_DEFINES

// Dokumentacja CAPD:
// http://mnich.ii.uj.edu.pl/capd/doc/example_intervals.html#ex_int_functions

// Uruchomienie: jestem w katalogu, w ktorym jest makefile i pozostale pliki;
// z konsoli: make (kompilacja); a potem ./cc-nb st * (uruchomienie z parametrami)
// uruchomienie z czasem i przekierowaniem: make && time ./cc-nb st * > rep.txt
// parametry st --- search i test (moze byc jeden z tych parametrow)
// * --- oznacza znak dodawany do nazwy plików z rozrzeszeniem .dat
// plik z danymi wejsciowymi musi nazywac sie init-data-*.txt --- ta gwiazdka to drugi parametr
// podawany przy uruchomieniu
// np.
// make && time ./cc-nb st 0 > rep.txt
// czyta z pliku init-data-0.txt, pisze do plikow cc-zeros-0.dat, cc-tested-zeros-0.dat i inter-data-0.dat
//
// Archiwizacja:   ./siup

#define MAX_ASYNC_DEPTH 8

#include <iostream>
#include <cstdio>
#include <list>
#include <time.h>

#include "cc-nb.h"
#include "solutions-tests.h"
#include "test-and-help-functions.h"
#include "estimates-functions.h"
#include "krawczyk-newton.h"

// File for binary output data
ofstream outFile;
ofstream interFile;

std::atomic<int> cnt(0);

char plikInit[30]; // plik z danymi wejściowymi
char plikZeros[30];
char plikInterData[30];
char plikZerosTested[30];

const MyInterval sqrt2 = sqrt(MyInterval(2.0));
const MyInterval sqrt3 = sqrt(MyInterval(3.0));

vector<MyDouble> xInit, yInit, massInit;

const short Counters = 9; // liczba testow
long licznik[10];         // tablica licznikow dla poszczegolnych testow:
                          // licznik[0] -- apriori
                          // licznik[1] -- closeBodies
                          // licznik[2] -- klaster
                          // licznik[3] -- distance (najdalszym cialem jest cialo q_{N-1})
                          // licznik[4] -- checkZero
                          // licznik[5] -- krawczyk zawiodl
                          // licznik[6] -- krawczyk znalazl
                          // licznik[7] -- krawczyk stwierdzil, ze nie ma zera
                          // licznik[8] -- spreadTest -- nie moga wszystkie ciala byc blizej niz 1
                          // licznik[9] -- checkUEqI
mutex updateCounter_mutex;

mutex krawczykSection_mutex;
mutex write_mutex;

atomic<long> clusterImprovementCounter(0);
atomic<long> interQuadCounter(0);

#include <map>
#include <string>

std::map<string, int> marks;

long methodFaileCounter = 0;
long methodZeroes = 0;
long collisionCounter = 0;
bool inicjuj = true;
bool searchStage; // searchStage = true -- faza szukania, false -- faza sprawdzania rozwiazan
// na etapie szukania zapisujemy wyniki do pliku binarnego; w fazie sprawdzania tylko cout
bool testStage;

//int pointInit = noChoice;
bool equalMasses;
bool orderIncreasing = false;
// it makes sense only for equalMasses, N > 4
// orderIncreasing = true: x2 <= x3 <= ... <= x_{n-3} <= x_{n-1} it makes sense only for equalMasses, N > 4
// orderIncreasing = false: x2 >= ... >= x_{n-3} >= x_{n-1}
bool fixedOrderOfBodies = true;
// in equalMasses case we assume a particular ordering of bodies according to orderIncreasing parameter
// otherwise we assume the body q_{n-2} (before the last) is the farthest body and x_{n-2} > 0
int normalizationMethod = equalToZero;
//MyDouble maxConfig; // maksymalna liczba roznych konfiguracji (2^i+1)^(2(N-1) -1), gdzie 2^ >= 2R/eps
//MyDouble kolejnyProcent = 1.0;
//MyDouble height;
clock_t startTime;
MyDouble bias;

const int MAX_ITER_NEWTON = 15;
int NUM_BODIES;
int MAT_DIM;

atomic<Count> globalZeros({0, 0});

MyInterval izero(0.0, 0.0);
MyDouble eps;
MyDouble eps1; // oddzielenie od zera, bliskosc do zderzenia.....

//ofstream logFile("log_file.txt");
/////////////////////////////////////////////////////

bool intersectEdgeWithLine(MyInterval coef, MyInterval qx, MyInterval qy, int edge,
                           MyInterval &pointx, MyInterval &pointy)
// direction = 1 --- prosta y = x*coef
// direction = -1 --- prosta y = -x*ceof
// edge: 0 -- lewa krawedz, 1 -- prawa, 2 -- dolna, 3 -- gorna
{
    //cout << "Hello! coef = " << coef << endl;
    MyInterval temp;
    bool intersect;
    //    cout << "qx = " << qx << ", qy = " << qy << ", kierunek = " << direction << endl;

    switch (edge)
    {
    case 0: // przeciecie z lewa krawedzia
        pointx = qx.leftBound();
        temp = pointx * coef;
        intersect = myIntersection(temp, qy, pointy);
        break;

    case 1: // przeciecie z prawa krawedzia
        pointx = qx.rightBound();
        temp = pointx * coef;
        intersect = myIntersection(temp, qy, pointy);
        break;

    case 2: // przeciecie z dolna krawedzia
        pointy = qy.leftBound();
        temp = pointy / coef;
        intersect = myIntersection(temp, qx, pointx);
        break;

    case 3: // przeciecie z gorna krawedzia
        pointy = qy.rightBound();
        temp = pointy / coef;
        intersect = myIntersection(temp, qx, pointx);
        break;

    default:
        intersect = false;
        break;
    }
    /*    cout << "wynik przeciecia " << intersect << endl;
    if (intersect)
    {
        cout << "punkt x = " << pointx << "punkt y = " << pointy << endl;
    }
    */
    return intersect;
}

MyInterval radiusXYn(MyInterval x, MyInterval y, int exp)
{
    return power(sqrt(power(x, 2) + power(y, 2)), exp);
}

void setMinMax(MyInterval x, MyInterval y,
               MyInterval &qMinX, MyInterval &qMaxX, MyInterval &qMinY, MyInterval &qMaxY,
               int rExp, int bExp)
{
    MyInterval r3 = radiusXYn(x, y, -rExp);
    MyInterval tempx = r3 * power(x, bExp);
    MyInterval tempy = r3 * power(y, bExp);
    if (tempx.leftBound() < qMinX)
        qMinX = tempx.leftBound();
    if (tempx.rightBound() > qMaxX)
        qMaxX = tempx.rightBound();

    if (tempy.leftBound() < qMinY)
        qMinY = tempy.leftBound();
    if (tempy.rightBound() > qMaxY)
        qMaxY = tempy.rightBound();
}

//******************************************************************************8*

void cuteCalculation(MyInterval x, MyInterval y, MyInterval &qx, MyInterval &qy,
                     int rExp, int bExp)
// liczymy wartosc q^bExp/|q|^{rExp}
// qx = x/r^{rExp}, qy = y/r^{rExp}
// bierzemy pod uwage proste:
// x = +/- sqrt(b/(r-b)) * y
// y  = +/- sqrt(b/(r-b)) * x
// x= 0
// y = 0
{
    if (rExp <= bExp)
    {
        cout << "Bledne wykladniki!";
        exit(1);
    }

    MyInterval coef = sqrt(MyInterval(bExp) / (rExp - bExp));
    MyInterval invCoef = 1 / coef;
    // coef -- wspolczynnik prostej, na ktorej sprawdzamy wartosci
    MyInterval qMinX, qMaxX, qMinY, qMaxY, qTemp;
    // najpierw sprawdzam wartosci w rogach kostki qi
    // lewy dolny rog --- inicjuje warosci qMin, qMax
    MyDouble xleft = x.leftBound(), xright = x.rightBound();
    MyDouble yleft = y.leftBound(), yright = y.rightBound();

    // lewy dolny
    MyInterval r3 = radiusXYn(xleft, yleft, -rExp); // r3 = |q|^{-rExp}
    MyInterval tempx = r3 * power(xleft, bExp);     // tempx = x^{bExp}/|q|^{rExp}
    MyInterval tempy = r3 * power(yleft, bExp);
    qMinX = tempx.leftBound();
    qMaxX = tempx.rightBound();
    qMinY = tempy.leftBound();
    qMaxY = tempy.rightBound();

    // lewy gorny
    setMinMax(xleft, yright, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // prawy gorny
    setMinMax(xright, yright, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // prawy dolny
    setMinMax(xright, yleft, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // teraz qMinX, qMaxX, qMinY, qMaxY -- min i max sposrod rogow
    MyInterval xI, yI;
    if ((xleft > 0 && yleft > 0)      // prostokat w prawej gornej cwiartce
        || (xright < 0 && yright < 0) // prostokat w lewej dolnej cwiartce
        || (isSingular(x)))
    {
        // przeciecie prostej y = x*coef z krawedziami kostki q
        for (int edge = 0; edge < 4; edge++)
        {
            if (intersectEdgeWithLine(coef, x, y, edge, xI, yI))
                setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        }

        // przeciecie prostej y = x/coef
        for (int edge = 0; edge < 4; edge++)
        {
            if (intersectEdgeWithLine(invCoef, x, y, edge, xI, yI))
                setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        }
    }

    if ((xright < 0 && yleft > 0)    // prostokat w lewej gornej cwiartce
        || (xleft > 0 && yright < 0) // prostokat w prawej dolnej cwiartce
        || (isSingular(y)))
    {
        // przeciecie prostej y = -x*coef
        for (int edge = 0; edge < 4; edge++)
        {
            if (intersectEdgeWithLine(-coef, x, y, edge, xI, yI))
                setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        }

        // przeciecie prostej y = -x/coef
        for (int edge = 0; edge < 4; edge++)
        {
            if (intersectEdgeWithLine(invCoef, x, y, edge, xI, yI))
                setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        }
    }

    // przeciecie z prosta x = 0
    if (isSingular(x))
    {
        MyDouble xAcc = 0.0;
        setMinMax(xAcc, yleft, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        setMinMax(xAcc, yright, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }
    // przeciecie z prosta y = 0
    if (isSingular(y))
    {
        MyDouble yAcc = 0.0;
        setMinMax(xleft, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
        setMinMax(xright, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }

    qx = MyInterval(qMinX.leftBound(), qMaxX.rightBound());
    qy = MyInterval(qMinY.leftBound(), qMaxY.rightBound());
}

//******************************************************************************8*

void cuteCalculationOldVersion(MyInterval x, MyInterval y, MyInterval &qx, MyInterval &qy,
                               int rExp, int bExp)
// liczymy wartosc q^bExp/|q|^{rExp}
// qx = x/r^{rExp}, qy = y/r^{rExp}
// bierzemy pod uwage proste:
// x = +/- sqrt(b/(r-b)) * y
// y  = +/- sqrt(b/(r-b)) * x
// x= 0
// y = 0
{
    if (rExp <= bExp)
    {
        cout << "Bledne wykladniki!";
        exit(1);
    }

    MyInterval coef = sqrt(MyInterval(bExp) / (rExp - bExp));
    // coef -- wspolczynnik prostej, na ktorej sprawdzamy wartosci
    MyInterval qMinX, qMaxX, qMinY, qMaxY, qTemp;
    // najpierw sprawdzam wartosci w rogach kostki qi
    // lewy dolny rog --- inicjuje warosci qMin, qMax
    MyDouble xAcc = x.leftBound();
    MyDouble yAcc = y.leftBound();
    MyInterval r3 = radiusXYn(xAcc, yAcc, -rExp); // r3 = |q|^{-rExp}
    MyInterval tempx = r3 * power(xAcc, bExp);    // tempx = x^{bExp}/|q|^{rExp}
    MyInterval tempy = r3 * power(yAcc, bExp);

    // lewy dolny
    qMinX = tempx.leftBound();
    qMaxX = tempx.rightBound();
    qMinY = tempy.leftBound();
    qMaxY = tempy.rightBound();
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;
    //setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY);

    // lewy gorny
    yAcc = y.rightBound();
    setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // prawy gorny
    xAcc = x.rightBound();
    setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // prawy dolny
    yAcc = y.leftBound();
    setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    //cout << "qMin = " << qMinX << ", qMinY = " << qMinY << endl;

    // teraz qMinX, qMaxX, qMinY, qMaxY -- min i max sposrod rogow
    MyInterval xI, yI;
    // przeciecie prostej y = x*coef z krawedziami kostki q
    for (int edge = 0; edge < 4; edge++)
    {
        if (intersectEdgeWithLine(coef, x, y, edge, xI, yI))
            setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }
    // przeciecie prostej y = -x*coef
    for (int edge = 0; edge < 4; edge++)
    {
        if (intersectEdgeWithLine(-coef, x, y, edge, xI, yI))
            setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }

    MyInterval invCoef = 1 / coef;
    // przeciecie prostej y = x/coef
    for (int edge = 0; edge < 4; edge++)
    {
        if (intersectEdgeWithLine(invCoef, x, y, edge, xI, yI))
            setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }

    // przeciecie prostej y = -x/coef
    for (int edge = 0; edge < 4; edge++)
    {
        if (intersectEdgeWithLine(invCoef, x, y, edge, xI, yI))
            setMinMax(xI, yI, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }

    // przeciecie z prosta x = 0
    if (isSingular(x))
    {
        xAcc = 0.0;
        yAcc = y.leftBound();
        setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);

        yAcc = y.rightBound();
        setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }
    // przeciecie z prosta y = 0
    if (isSingular(y))
    {
        yAcc = 0.0;
        xAcc = x.leftBound();
        setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);

        xAcc = x.rightBound();
        setMinMax(xAcc, yAcc, qMinX, qMaxX, qMinY, qMaxY, rExp, bExp);
    }

    qx = MyInterval(qMinX.leftBound(), qMaxX.rightBound());
    qy = MyInterval(qMinY.leftBound(), qMaxY.rightBound());
}

//********************************************************************

/////////////////////////////////////////////////////////////////////
// Bodies functions definitions
////////////////////////////////////////////////////////////////////

MyDouble Bodies::maxDiam(int &max_i, int &coord)
{
    // szukamy srednicy najdluszego przedzialu
    // znaleziony przedzial opisuje cialo max_i-te, coord mowi, czy nalezy dzielic x, czy y
    max_i = 0;
    coord = 0; // coord = 0 dzielimy x, coord = 1 dzielimy y
    MyDouble maxLengthInterval = diam(x(0));
    for (int i = 0; i <= N - 2; i++)
    {
        if (diam(x(i)) > maxLengthInterval)
        {
            max_i = i;
            coord = 0;
            maxLengthInterval = diam(x(i));
        };
        if (diam(y(i)) > maxLengthInterval)
        {
            max_i = i;
            coord = 1;
            maxLengthInterval = diam(y(i));
        }
    }

    return maxLengthInterval;
}

MyDouble Bodies::minDiam(int &min_i, int &coord)
{
    // szukamy srednicy najdluszego przedzialu
    // znaleziony przedzial opisuje cialo max_i-te, coord mowi, czy nalezy dzielic x, czy y
    min_i = 0;
    coord = 0; // coord = 0 dzielimy x, coord = 1 dzielimy y
    MyDouble minLengthInterval = diam(x(0));
    for (int i = 0; i <= N - 2; i++)
    {
        if (diam(x(i)) < minLengthInterval)
        {
            min_i = i;
            coord = 0;
            minLengthInterval = diam(x(i));
        };
        if (diam(y(i)) < minLengthInterval && i != N - 2)
        {
            min_i = i;
            coord = 1;
            minLengthInterval = diam(y(i));
        }
    }

    return minLengthInterval;
}

MyInterval Bodies::xN() const
{

    MyInterval sum = izero;
    for (int i = 0; i < N - 1; ++i)
    {
        Body b = data[i];
        sum += b.mass * b.x;
    }

    return -sum / massN;
}

MyInterval Bodies::yN() const
{
    MyInterval sum = izero;
    for (int i = 0; i < N - 1; ++i)
    {
        Body b = data[i];
        sum += b.mass * b.y;
    }

    return -sum / massN;
}

void Bodies::setX(int i, MyInterval val)
{
    if (i >= N - 1)
    {
        cout << "index_out_of_bounds" << endl;
        exit(1);
    }

    data[i].x = val;
}

void Bodies::setY(int i, MyInterval val)
{

    if (i >= N - 1)
    {
        cout << "index_out_of_bounds" << endl;
        exit(1);
    }
    switch (normalizationMethod)
    {
    case equalToFirst:
        // if (i >= N-1) _error(index_out_of_bounds, blabla);
        data[i].y = val;
        if (i == 0)
            data[N - 2].y = data[0].y; // y_1 = y_{n-1}
        if (i == N - 2)
            data[0].y = data[N - 2].y;

        break;

    case equalToZero:
        data[i].y = val;
        if (i == N - 2)
            data[N - 2].y = 0.0;

        break;

    default:
        break;
    }
}

bool Bodies::collisionHappened()
{
    // parametry i, j maja zakres od 1 do DIM
    for (int i = 0; i < N; i++)
        for (int j = i + 1; j < N; j++)
        {
            if (isSingular(radius(i, j)))
                return true;
        }
    return false;
}

bool Bodies::GX(int i, MyInterval &sum)
// computes x-coordinate of \sum_{j,j\neq i} \frac{m_j}{r_{ij}^3}(q_i - q_j)
{

    sum = 0.0;
    for (int j = 0; j <= N - 1; ++j)
    {
        if (j == i)
            continue;
        MyInterval r = radius(i, j);
        if (isSingular(r)) // kolizja miedzy cialami i, j
            return false;

        MyInterval qx, qy;
        cuteCalculation(distX(j, i), distY(j, i), qx, qy, 3, 1); // distX/|q|^3

        sum += mass(j) * qx;
    }
    return true;
}

bool Bodies::fX(int i, MyInterval &fXi, MyInterval &gXi)
{

    if (GX(i, gXi))
    {
        fXi = x(i) - gXi;
        return true;
    }
    else // byla kolizja miedzy cialem i, a jakim innym --- MOZNABY ZWRACAC INFO: jakim innym
        return false;
}

bool Bodies::GY(int i, MyInterval &sum)
// computes y-coordinate of \sum_{j,j\neq i} \frac{m_j}{r_{ij}^3}(q_i - q_j)
{
    sum = 0.0;

    for (int j = 0; j <= N - 1; ++j)
    {
        if (j == i)
            continue;
        MyInterval r = radius(i, j);
        if (isSingular(r)) // kolizja miedzy cialami i, j
            return false;

        MyInterval qx, qy;
        cuteCalculation(distX(j, i), distY(j, i), qx, qy, 3, 1);
        sum += mass(j) * qy;
    }
    return true;
}

bool Bodies::fY(int i, MyInterval &fYi, MyInterval &gYi)
{
    if (GY(i, gYi))
    {
        fYi = y(i) - gYi;
        return true;
    }
    else
        return false;
}

/* df^x_i/dx_k  */
MyInterval Bodies::DxfX(int i, int k, MyMatrix &dpxM)
{
    MyInterval sum(0.0, 0.0);

    if (i == k)
    {
        for (int j = 0; j < N - 1; j++)
        {
            if (j == i)
                continue;
            sum += mass(j) * dpxM[i][j];
        }
        sum += mass(N - 1) * (1 + mass(i) / mass(N - 1)) * dpxM[i][N - 1];
        return 1.0 - sum;
    }
    else
        return mass(k) * (dpxM[i][k] - dpxM[i][N - 1]);
}

/* df^y_i/dy_k  */
MyInterval Bodies::DyfY(int i, int k, MyMatrix &dpyM)
{
    MyInterval sum(0.0, 0.0);

    if (i == k)
    {
        for (int j = 0; j < N - 1; j++)
        {
            if (j == i)
                continue;
            sum += mass(j) * dpyM[i][j];
        }
        sum += mass(N - 1) * (1 + mass(i) / mass(N - 1)) * dpyM[i][N - 1];
        return 1.0 - sum;
    }
    else
    {
        return mass(k) * (dpyM[i][k] - dpyM[i][N - 1]);
    }
}

/* df_i^x/dy_k = df_i^y/dx_k  */
MyInterval Bodies::DxfY(int i, int k, MyMatrix &ppM)
{
    MyInterval sum(0.0, 0.0);

    if (i == k)
    {
        for (int j = 0; j < N - 1; j++)
        {
            if (j == i)
                continue;
            sum += mass(j) * ppM[i][j];
        }
        sum += mass(N - 1) * (1 + mass(i) / mass(N - 1)) * ppM[i][N - 1];
        return sum;
    }
    else
    {
        return mass(k) * (ppM[i][N - 1] - ppM[i][k]);
    }
}

void Bodies::printData(bool writeDistFromZero)
{
    for (int i = 0; i < N; ++i)
    {
        cout << "i: " << i << " ";
        cout << "X: " << x(i) << " ";
        cout << "Y: " << y(i) << " ";
        cout << "mass: " << mass(i) << " ";
        cout << endl;
    }
    if (writeDistFromZero)
    {
        cout << "Distances from 0:" << endl;
        for (int i = 0; i < N; i++)
        {
            MyInterval xi = power(x(i), 2);
            MyInterval yi = power(y(i), 2);
            cout << i << ":    " << sqrt(xi + yi) << endl;
        }
    }
}

void Bodies::printDataToLog()
{
    // for (int i = 0; i < N; ++i)
    // {
    //     logFile << "i: " << i << " ";
    //     logFile << "X: " << x(i) << " ";
    //     logFile << "Y: " << y(i) << " ";
    //     //logFile << "mass: " << mass(i) << " ";
    //     logFile << endl;
    // }
    // logFile << endl
    //         << flush;
}

/*
void Bodies::printDiam()
{
    for (int i = 0; i < N; ++i)
    {
        cout << "i: " << i << " ";
        cout << "diam(X): " << diam(x(i)) << " ";
        cout << "diam(Y): " << diam(y(i)) << " ";
        cout << endl
             << flush;
    }
}
*/

void Bodies::printDiam()
// teraz wypisuje tylko duze przedzialy -- procedura TYMCZASOWA!!!
{
    //    MyDouble sizeOfCube = 1.0;
    //    for (int i = 0; i < N; ++i)
    //        sizeOfCube *= diam(x(i)) * diam(y(i));

    for (int i = 0; i < N; ++i)
    {
        cout << "i: " << i << " ";
        cout << "diam(X): " << diam(x(i)) << " ";
        cout << "diam(Y): " << diam(y(i)) << " ";
        cout << endl
             << flush;
    }
    cout << endl;
}

void Bodies::writeBinaryData(ostream &outFile)
// do pliku binarnego pisze tylko w fazie szukania rozwiazan, czyli gdy searchStage = true
{
    outFile.write((char *)&N, sizeof(int)); // sizeof can take a type
    outFile.write((char *)&massN, sizeof(MyInterval));

    for (int i = 0; i < N - 1; i++)
    {
        outFile.write((char *)&(data[i]), sizeof(Body));
    }
    outFile.flush();
}

bool Bodies::readBinaryData(istream &inFile)
// zwraca false, jesli nie przeczytal
{
    while (!data.empty())
        data.pop_back();

    inFile.read((char *)&N, sizeof(int)); // sizeof can take a type
    if (inFile.gcount() == 0)             // przeczytana liczba bajtow
        return false;

    //cout << "przeczytana liczba cial = " << N << endl;
    inFile.read((char *)&massN, sizeof(MyInterval));

    Body temp;
    for (int i = 0; i < N - 1; i++)
    {
        inFile.read((char *)&temp, sizeof(Body));
        data.push_back(temp);
    }
    return true;
}

void Bodies::printResult(std::ostream &resFile, bool silent)
{
    if (silent)
        return;

    for (int i = 0; i < N - 1; i++)
    {
        resFile << x(i).leftBound() << ";" << x(i).rightBound() << ";";
        resFile << y(i).leftBound() << ";" << y(i).rightBound() << ";";
    }

    resFile << x(N - 1).leftBound() << ";" << x(N - 1).rightBound() << ";";
    resFile << y(N - 1).leftBound() << ";" << y(N - 1).rightBound() << ";";
    resFile << "\n";
}

Bodies *Bodies::calculateMidBodies()
{
    MyDouble leftPoint, rightPoint, midPoint;
    Bodies *midBodies = new Bodies(*this);

    for (int i = 0; i < N - 1; i++)
    {
        leftPoint = x(i).leftBound();
        rightPoint = x(i).rightBound();
        midPoint = (leftPoint + rightPoint) / 2.0;
        midBodies->setX(i, midPoint);

        leftPoint = y(i).leftBound();
        rightPoint = y(i).rightBound();
        midPoint = (leftPoint + rightPoint) / 2.0;
        midBodies->setY(i, midPoint);
    }

    return midBodies;
}

//////////////////////////////////////////////////////////////////////////

void calculatePuffedBodies(Bodies bodies, MyInterval addedInterval, Bodies &puffedBodies)
// rozdmuchuje boksik kazdego ciala
{
    int beforeLast = bodies.N - 2;
    for (int i = 0; i < bodies.N - 1; i++)
    // ostatniego ciala nie modyfikujemy -- wylicza sie automatycznie
    {
        puffedBodies.setX(i, bodies.x(i) + addedInterval);
        puffedBodies.setY(i, bodies.y(i) + addedInterval);
    }

    if (normalizationMethod == equalToZero)
    {
        puffedBodies.setY(beforeLast, 0.0);
    }
    else // normalizationMethod == equalTo First
    {
        puffedBodies.setY(beforeLast, puffedBodies.y(0));
    }
}

/////////////////////////////////

// //////////////////////////////////////////////////////////
// // ALGORYTM PODZIALU
// //////////////////////////////////////////////////////////

Count blowUp(Bodies bodies)
// dla bodies o malej srednicy funkcja
// 1) sprawdza, czy pochodna w punkcie srodkowy jest izomorfizmem
// 2) jesli tak, to rozdmuchuje bodies i probuje udowodnic lub wykluczyc istnienie rozw. w zbiorze
// 2) jesli nie, to wiciaz potencjalne zero
{
    Count result = {0, 0};
    int MAT_DIM = 2 * (bodies.N - 1);
    bool singularMatrix = false;
    //int sizeOfBodies = sizeof(bodies);

    MyMatrix DF(MAT_DIM, MAT_DIM), DFF(MAT_DIM - 1, MAT_DIM - 1), invDF(MAT_DIM - 1, MAT_DIM - 1);
    Bodies *midBodies = bodies.calculateMidBodies();
    //////////////

    fillDFMatrix(*midBodies, DF);

    newMatrix(DF, DFF);

    MyMatrix midDF = capd::vectalg::midMatrix(DFF);
    try
    {
        MyMatrix invMidDF = capd::matrixAlgorithms::gaussInverseMatrix(midDF); // DF11 = mid^{-1}
    }
    catch (std::runtime_error)
    {
        singularMatrix = true;
    }

    if (!singularMatrix)
    {
        int trash1 = 0, trash2 = 0;
        MyDouble maxLengthInterval = bodies.maxDiam(trash1, trash2);
        MyInterval half = MyInterval(maxLengthInterval) / 2.0;
        half = MyInterval(-half.rightBound(), half.rightBound());
        //        cout << "polowka najdluzszego przedzialu = " << half << endl;
        Bodies puffedBodies(bodies);
        calculatePuffedBodies(*midBodies, half, puffedBodies);

        //cout << "Pierwsze rozdmuchane dane:" << endl;
        //puffedBodies.printData();
        //puffedBodies.printDiam();

        bool intersec;

        ResultType newtonRes = methodFailed;
        Bodies ala(bodies.N);
        newtonRes = (methodUsed == usedNewton) ? newtonMethod(bodies, ala, intersec)
                                               : krawczykMethod(bodies, ala, intersec);
        switch (newtonRes)
        {
        case methodFailed:
            //cout << "krawczyk nie zadzialal" << endl;
            //-----------------------------
            break; // newton bezużyteczny

        case uniqueZero:
            //cout << methodZeroes << "-------------------------- NEWTON/Krawczyk znalazl miejsce zerowe dla danych:";
            //printAllData(ala);
            methodZeroes++;

            //bodies.writeBinaryData(outFile);
            if (searchStage)
                outFile.open(plikZeros, ios::binary | ios::out | ios::app);
            else
                outFile.open(plikZerosTested, ios::binary | ios::out | ios::app);
            ala.writeBinaryData(outFile); // uwaga!!!
            outFile.close();

            //outFile.write((char *)&bodies, sizeOfBodies); // sizeof can take a type
            result.zeros = 1;
            result.maybeZeros = 0;
            break;

        case noZeroInSet:
            cout << "Nie ma zera" << endl;
            result.zeros = 0;
            result.maybeZeros = 0;
            break; // nie ma zer

        default:
            break;
        }
        //exit(1);
        //----------------------------
        bool theEnd = false;
        while ((newtonRes != uniqueZero) && (!theEnd))
        {

            //cout << "petla dziala!" << endl;
            // nie powinno byc kolizji, ale na wszelki wypadel sprawdzamy
            bool collision = bodies.collisionHappened();
            bool intersec;

            if (!collision)
            {
                Bodies tmp(puffedBodies.N);
                newtonRes = (methodUsed == usedNewton) ? newtonMethod(puffedBodies, tmp, intersec)
                                                       : krawczykMethod(puffedBodies, tmp, intersec);

                switch (newtonRes)
                {
                case methodFailed:
                    //-------------------------
                    maxLengthInterval = puffedBodies.maxDiam(trash1, trash2);
                    if (maxLengthInterval >= 1e-2)
                    {
                        theEnd = true;
                    }
                    else
                    {
                        half = MyInterval(maxLengthInterval) / 2.0;
                        half = MyInterval(-half.rightBound(), half.rightBound());
                        calculatePuffedBodies(puffedBodies, half, puffedBodies);
                    }
                    //-----------------------------
                    break; // newton bezużyteczny

                case uniqueZero:
                    //cout << methodZeroes << "-------------------------- NEWTON/Krawczyk znalazl miejsce zerowe dla danych:";
                    //printAllData(tmp);
                    methodZeroes++;

                    //bodies.writeBinaryData(outFile);
                    if (searchStage)
                        outFile.open(plikZeros, ios::binary | ios::out | ios::app);
                    else
                        outFile.open(plikZerosTested, ios::binary | ios::out | ios::app);
                    tmp.writeBinaryData(outFile); // uwaga!!!
                    outFile.close();

                    //                    outFile.write((char *)&bodies, sizeOfBodies); // sizeof can take a type
                    result.zeros = 1;
                    result.maybeZeros = 0;
                    break;

                case noZeroInSet:
                    theEnd = true;
                    cout << "Nie ma zera" << endl;
                    //puffedBodies.printData();
                    //cout << endl;
                    result.zeros = 0;
                    result.maybeZeros = 0;
                    break; // nie ma zer

                default:
                    break;
                }
            }
        } // while
    }     // if (!singularMatrix)

    delete midBodies;
    return result;
}

Count searchAsync(Bodies bodies, int depth)
{

    MyDouble leftPoint, rightPoint, midPoint, leftMidPoint, rightMidPoint;
    ResultType newtonRes = uniqueZero;
    Count result = {0, 0};
    Count leftCount, rightCount;
    std::future<Count> leftCountF, rightCountF;
    bool shouldAsync = cnt < MAX_ASYNC_DEPTH;
    /*
    write_mutex.lock();
    if (shouldAsync)
        cout << "searchAsync with level: " << cnt << std::endl
             << std::flush;
    write_mutex.unlock();
*/
    int newDepth = shouldAsync ? depth + 1 : depth;

    bool collision = bodies.collisionHappened();

    // szukamy najdluszego przedzialu do podzialu
    // znaleziony przedzial opisuje cialo max_i-te, coord mowi, czy nalezy dzielic x, czy y
    int max_i = 0;
    int coord = 0; // coord = 0 dzielimy x, coord = 1 dzielimy y
    MyDouble maxLengthInterval = bodies.maxDiam(max_i, coord);

    if (!thereIsNoSolution(bodies, licznik, collision, result))
    {
        return result;
    }

    bool intersec = false;

    if (!collision)
        if (maxLengthInterval < bias)
        {
            // ??? potencjalne miejsce zerowe z dokladnoscia eps;
            // sprawdzamy metoda Newtona, czy moze tu byc zero

            Bodies tmp(NUM_BODIES);

            newtonRes = (methodUsed == usedNewton) ? newtonMethod(bodies, tmp, intersec)
                                                   : krawczykMethod(bodies, tmp, intersec);

            switch (newtonRes)
            {
            case methodFailed:
            {
                //cout << "newton bezużyteczny" << endl;
                krawczykSection_mutex.lock();
                if (searchStage)
                    licznik[5]++;
                krawczykSection_mutex.unlock();
                ///*
                if (intersec)
                {
                    bodies = tmp;
                }
                //*/
            }
            //-----------------------------
            break; // newton bezużyteczny

            case uniqueZero:
                krawczykSection_mutex.lock();
                if (searchStage)
                    licznik[6]++;

                methodZeroes++;

                if (searchStage)
                    outFile.open(plikZeros, ios::binary | ios::out | ios::app);
                else
                    outFile.open(plikZerosTested, ios::binary | ios::out | ios::app);
                tmp.writeBinaryData(outFile); // uwaga!!!
                outFile.close();
                krawczykSection_mutex.unlock();

                result.zeros = 1;
                result.maybeZeros = 0;
                return result;
                break;

            case noZeroInSet:
                result.zeros = 0;
                result.maybeZeros = 0;
                krawczykSection_mutex.lock();
                if (searchStage)
                    licznik[7]++;
                krawczykSection_mutex.unlock();
                return result;
                break; // nie ma zer

            default:
                break;
            }
        }

    if (maxLengthInterval < eps)
    // if the size of the cube is too small, then we interrupt search
    {
        if (newtonRes == methodFailed)
        {
            // bodies.printResult(resultFile, silent);
            //cout << "Potencjalne miejsce zerowe -- Newton/Krawczyk nie rozstrzygnal" << endl;
            //bodies.printData();
            //bodies.printDiam();

            krawczykSection_mutex.lock();
            methodFaileCounter++;
            krawczykSection_mutex.unlock();
        }
        result.zeros = 0;
        result.maybeZeros = 1;
        return result;
    }

    MyDouble zakladka = 1e-5; // zakladka musi byc mniejsza od 0.5
    switch (coord)
    {
    case 0:
        leftPoint = bodies.x(max_i).leftBound();
        rightPoint = bodies.x(max_i).rightBound();
        midPoint = (leftPoint + rightPoint) / 2.0;
        rightMidPoint = midPoint + (rightPoint - leftPoint) * zakladka;
        leftMidPoint = midPoint - (rightPoint - leftPoint) * zakladka;
        bodies.setX(max_i, MyInterval(leftPoint, rightMidPoint));
        break;

    case 1:
        leftPoint = bodies.y(max_i).leftBound();
        rightPoint = bodies.y(max_i).rightBound();
        midPoint = (leftPoint + rightPoint) / 2.0;
        rightMidPoint = midPoint + (rightPoint - leftPoint) * zakladka;
        leftMidPoint = midPoint - (rightPoint - leftPoint) * zakladka;
        bodies.setY(max_i, MyInterval(leftPoint, rightMidPoint));

        break;

    default:
        break;
    }

    /*
    clock_t searchLocalTime;
    searchLocalTime = clock();
    */
    auto policy = std::launch::async;
    if (shouldAsync)
    {

        leftCountF = std::async(policy, searchAsync, bodies, newDepth);
        cnt += 1;
    }
    else
    {
        leftCount = searchAsync(bodies, newDepth);
    }

    switch (coord)
    {
    case 0:
        bodies.setX(max_i, MyInterval(leftMidPoint, rightPoint));
        break;

    case 1:
        bodies.setY(max_i, MyInterval(leftMidPoint, rightPoint));
        break;

    default:
        break;
    }
    //searchLocalTime = clock();

    if (shouldAsync)
    {
        rightCountF = std::async(policy, searchAsync, bodies, newDepth);
        leftCount = leftCountF.get();
        rightCount = rightCountF.get();
        cnt -= 1;
    }
    else
    {
        rightCount = searchAsync(bodies, newDepth);
    }

    result.zeros = leftCount.zeros + rightCount.zeros;
    result.maybeZeros = leftCount.maybeZeros + rightCount.maybeZeros;
    return result;

} //search

Count search(Bodies bodies)
{
    return searchAsync(bodies, 0);
}

// //----------------------------------------------------------------------------------------------------

void testCzytania(int NUM_BODIES)
{
    vector<Bodies> ccSolutions;
    vector<int> markedSolutions;
    int uniqueSol;
    cout << "Testowanie czytania!" << endl;
    int ccNo = readBodiesFromBinaryFile(plikZerosTested, ccSolutions);
    cout << "Wczytana liczba cc = " << ccNo << endl;

    testujZera(plikZerosTested, ccNo, ccSolutions, markedSolutions, uniqueSol);
    cout << "TUTAJ -- test czytania" << endl;

    //exit(1);
}

void duplicateElimination(const char *nazwaPliku, bool sym, int &uniqueSol)
// sym = true --- testuje symetrie
// sym = false -- sparwdza tylko unikalnosc rozwiazan
// uniqueSol -- liczba unikalnych rozwiazan
{
    int ccNo;
    vector<Bodies> ccSolutions;
    vector<int> markedSolutions;

    if ((ccNo = readBodiesFromBinaryFile(nazwaPliku, ccSolutions)) > 0)
    {
        testujZera(nazwaPliku, ccNo, ccSolutions, markedSolutions, uniqueSol);
        if (!sym)
        {
            cout << "ccSolutions = " << ccNo << endl;
        }
        if (sym)
            testujSymetrie(ccNo, ccSolutions, markedSolutions);
    }
    else
    {
        uniqueSol = 0;
        cout << "Nie znaleziono CC!";
    }
}

int initParameters()
{
    int DIM;
    MyDouble temp;
    char trash[240];

    std::fstream plik;
    xInit.clear();
    yInit.clear();
    massInit.clear();
    plik.open(plikInit, std::ios::in);
    if (plik.good() == true)
    {
        //std::cout << "Uzyskano dostep do pliku!" << std::endl;
        //tu operacje na pliku
        plik >> equalMasses;
        plik.getline(trash, 240); // to eat endl - and the eventual comment following
                                  // the dimension
        plik >> DIM;
        plik.getline(trash, 240);
        //cout << "Liczba cial = " << DIM << endl;

        for (int i = 0; i < DIM - 1; i++)
        {
            if (plik.eof())
            {
                cout << "Wrong input data -- x!" << endl;
                exit(3);
            }
            plik >> temp;
            xInit.push_back(temp);
            //            cout << "wczytano x " << i << ": " << temp;
            plik >> temp;
            xInit.push_back(temp);
            //            cout << ", " << temp << endl;
        }
        plik.getline(trash, 240);

        for (int i = 0; i < DIM - 1; i++)
        {
            if (plik.eof())
            {
                cout << "Wrong input data -- y!" << endl;
                exit(3);
            }
            plik >> temp;
            yInit.push_back(temp);
            //            cout << "wczytano y " << i << ": " << temp;
            plik >> temp;
            yInit.push_back(temp);
            //            cout << ", " << temp << endl;
        }
        plik.getline(trash, 240);

        if (equalMasses)
        {
            for (int i = 0; i < DIM - 1; i++)
            {
                temp = 1.0 / DIM;
                massInit.push_back(temp);
                massInit.push_back(temp);
                //            cout << "wczytano mase " << i << ": " << temp;
            }
        }
        else
        {
            MyDouble massSum = 0.0;
            for (int i = 0; i < DIM - 1; i++)
            {
                if (plik.eof())
                {
                    cout << "Wrong input data -- m!" << endl;
                    exit(3);
                }
                plik >> temp;
                massInit.push_back(temp);
                //cout << "wczytano mase " << i << ": " << temp;
                plik >> temp;
                massInit.push_back(temp);
                massSum += temp;
                //cout << "wczytano mase " << i << ": " << temp;
            }
            plik.getline(trash, 240);
            if (massSum > 1)
            {
                cout << "Masa " << massSum << endl;
                cout << "Wrong input data!" << endl;
                exit(3);
            }
        }

        plik.close();
    }
    else
    {
        std::cout << "Brak dostepu do pliku z danymi!" << std::endl;
        exit(1);
    }

    //    const int DIM = 4; // number of bodies
    methodUsed = usedKrawczyk;

    // zeby normalizacja equalToZero mogla dzialac dane poczatkowe musza byc z y_{n-1} = 0!!!
    normalizationMethod = equalToZero; // y_{n-1} = 0

    //normalizationMethod = equalToFirst; // y_{n-1} = y_0
    eps = 1e-5;
    //eps1 = ((1.0 / DIM) * (1.0 / DIM)) / ((DIM - 1) * (DIM - 1)); // four bodies, equal masses, lower bound estimate for distance
    MyDouble R = DIM - 1;           // tu powinno byc oszacowanie apriori, nie wiecej niz DIM-1
    eps1 = 1 / (DIM * DIM * R * R); // 1/DIM -- masa w przypadku rownych cial

    //    maxConfig = power(2 * R / eps, 2 * (DIM - 1) - 1);
    // dla 8 cial --- zeby cokolwiek wypisywal:
    //maxConfig = power(8 / eps, 7);
    //cout << "Maksymalna liczba konfiguracji = " << maxConfig << endl;

    /*
    try
    {
        height = log2(maxConfig);
        cout << "Wysokosc drzewa poszukiwan = " << height << endl;
    }
    catch (...)
    {
        cout << "nie powiodlo sie obliczenie wysokosci drzewa poszukiwan!" << endl;
    }
*/
    return DIM;
}

void przeliczCzas(clock_t timeLength)
{

    cout << "Program computed " << (((double)timeLength) / CLOCKS_PER_SEC) / 60 << " minutes" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        cerr << "Zla liczba parametrow" << endl;
        exit(1);
    }

    char idi = *argv[2]; // identyfikator instancji
    if (idi < '0' || idi > '9')
    {
        cerr << "Zly identyfikator instancji" << endl;
        exit(1);
    }

    sprintf(plikInit, "init-data-%s-%s.txt", argv[2], argv[3]);
    sprintf(plikZeros, "cc-zeros-%s-%s.dat", argv[2], argv[3]);
    sprintf(plikZerosTested, "cc-tested-zeros-%s-%s.dat", argv[2], argv[3]);
    sprintf(plikInterData, "inter-data-%s-%s.dat", argv[2], argv[3]);

    NUM_BODIES = initParameters();
    if (atoi(argv[2]) != NUM_BODIES)
    {
        cout << "Zle dane w pliku!" << endl;
        exit(2);
    }

    sprintf(plikZeros, "cc-zeros-%d-%s.dat", NUM_BODIES, argv[3]);
    sprintf(plikZerosTested, "cc-tested-zeros-%d-%s.dat", NUM_BODIES, argv[3]);
    sprintf(plikInterData, "inter-data-%d-%s.dat", NUM_BODIES, argv[3]);

    MAT_DIM = 2 * (NUM_BODIES - 1);

    Bodies bodies(NUM_BODIES, true);

    // Ustawiamy searchStage na flase (czyli tryb testowy), jesli podano parametr 't' (jak "test")
    char *par = argv[1];
    searchStage = par[0] == 's';
    testStage = par[0] == 't' || par[1] == 't';

    //lookInsideOfInterData();
    //exit(1);

    std::cout.precision(10);
    cout << "Number of bodies = " << NUM_BODIES << endl;
    /*
    if (methodUsed == usedKrawczyk)
        cout << "KRAWCZYK's method" << endl;
    else
        cout << "NEWTON's method" << endl;
*/
    cout << "Accuracy eps = " << eps;
    bias = 1e-2;
    if (NUM_BODIES <= 4)
        bias = 1e-2;

    cout << ", bias = " << bias << endl;
    cout << "MAX_ASYNCH_DEPTH = " << MAX_ASYNC_DEPTH << endl;

    /*
    cout << "MeanValueOption ";
    if (meanValueOption)
        cout << "ON" << endl;
    else
        cout << "OFF" << endl;
*/
    cout << endl;
    cout << "Input data:" << endl;
    if (!bodies.collisionHappened())
    {
        // jezeli jest kolizja, to pojawia sie dzielenie przez 0
        // liczymy GX, gdzie jest power(radius, -3) -- jesli radius zawadza o zero, to dzielimy przez 0
        cout << "Wartosci poczatkowe: " << flush;
        printAllData(bodies);
    }
    else
    {
        //cout << "KOLIZJA!" << endl;
        bodies.printData();
        cout << endl;
    }

    // inicuje wszystkie liczniki testow na 0
    for (int i = 0; i < Counters; i++)
    {
        licznik[i] = 0;
    }

    if (searchStage)
    {
        /** Printing to files */
        //ofstream resFile("result.csv");

        outFile.open(plikZeros, ios::binary | ios::out | ios::trunc);
        outFile.close();

        //inline void initLogFile(ostream logFile) { logFile << "Info cc-nb:\n";}
        //resFile << "x1;;y1;;x2;;y2;;x3;;y3\n";
        //resFile.precision(25);
        // logFile.precision(25);

        //MpFloat::setDefaultPrecision(100);

        //time_t rawtime;
        //struct tm *timeinfo;

        clock_t timeLength = clock();
        //time(&rawtime);
        //timeinfo = localtime(&rawtime);
        //cout << "Init time: " << asctime(timeinfo) << endl;

        try
        {
            globalZeros = search(bodies);
            //globalZeros = blowUp(bodies);
        }
        catch (...)
        {
            cout << "Wyjatek" << endl;
        }

        cout << endl;
        //cout << "Liczba kolizji: " << collisionCounter << endl;
        cout << "The number of undecided cubes: " << methodFaileCounter << endl;
        cout << "The number of zeros in the method: " << methodZeroes << endl;
        //cout << "Number of potential zeros: " << globalZeros.maybeZeros << endl; // policzone za male przedzialy
        //resFile.close();
        //outFile.close(); // there are all solutions in this file

        timeLength = clock() - timeLength;
        //time(&rawtime);
        //timeinfo = localtime(&rawtime);
        //cout << "Czas koncowy: " << asctime(timeinfo) << endl
        //     << endl;
        przeliczCzas(timeLength);
        cout << endl;
        searchStage = false; // zmieniam tryb wypisywania -- tylko cout

        // licznik[0] -- apriori
        // licznik[1] -- closeBodies
        // licznik[2] -- klaster
        // licznik[3] -- distance (najdalszym cialem jest cialo q_{N-1})
        // licznik[4] -- checkZero
        // licznik[5] -- krawczyk methodFailed
        // licznik[6] -- krawczyk zeroIside
        // licznik[7] -- krawczyk noZeroInside
        cout << "Tests usage:" << endl;
        cout << "checkAprioriBounds -- " << licznik[0] << endl;
        //cout << "closeBodiesTest -- " << licznik[1] << endl;
        cout << "clusterTest -- " << licznik[2] << endl;
        cout << "distanceTest -- " << licznik[3] << endl;
        cout << "checkZero -- " << licznik[4] << endl;
        cout << "krawczyk: methodFailed  -- " << licznik[5] << endl;
        cout << "krawczyk: zeroIside -- " << licznik[6] << endl;
        cout << "krawczyk: no zero inside -- " << licznik[7] << endl;
        cout << "spreadTest -- " << licznik[8] << endl;
        cout << "U = I -- " << licznik[9] << endl;
        cout << "improvement in clusterTest -- " << clusterImprovementCounter << endl;
        cout << "improvement in U = I Test -- " << interQuadCounter << endl;
    }
    /*
    int prog = 1000;
    for (std::map<string, int>::iterator it = marks.begin(); it != marks.end(); ++it)
    {
        if (it->second >= prog)
            std::cout << it->first << " => " << it->second << '\n';
    }
*/
    // w fazie testowania plik cc-zeros.dat jest plikiem do czytania
    // cc-tested-zeros.dat plikiem do pisania
    ///*
    if (testStage)
    {
        outFile.open(plikZerosTested, ios::binary | ios::out | ios::trunc);
        outFile.close();
        //*/
        int uniqueSol = 0;
        bool symmetryTest = true;
        duplicateElimination(plikZeros, symmetryTest, uniqueSol);
    }
    return 0;
}
