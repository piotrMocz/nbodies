all: cc-nb

cc-nb: find-symmetry.o solutions-tests.o test-and-help-functions.o estimates-functions.o krawczyk-newton.o cc-nb.o          
	g++ -std=c++11 -lpthread -pthread -O2 -g -Wall find-symmetry.o solutions-tests.o test-and-help-functions.o estimates-functions.o krawczyk-newton.o cc-nb.o -o cc-nb `mpcapd-config --libs`

krawczyk-newton.o: cc-nb.h
test-and-help-functions.o: cc-nb.h 
estimates-functions.o: cc-nb.h
solutions-tests.o: cc-nb.h
find-symmetry.o: cc-nb.h

%.o:%.cpp %.h
	g++ -std=c++11 -lpthread -pthread -O2 -g -Wall `mpcapd-config --cflags` -c $< -o $@

