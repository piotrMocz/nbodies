#ifndef ccnb_h
#define ccnb_h

#include "capd/capdlib.h"
#include "capd/mpcapdlib.h"
#include <capd/vectalg/Vector_inline.h>
#include <vector>
#include <future>
#include <thread>

using namespace std;
using namespace capd;

///*
// pojedyncza precyzja obliczen
typedef capd::Interval MyInterval;
typedef double MyDouble;
typedef capd::IMatrix MyMatrix;
typedef capd::IVector MyVector;
//*/

/*
// multiple precission
typedef capd::MpInterval MyInterval;
typedef capd::MpFloat MyDouble;
typedef capd::MpIMatrix MyMatrix;
typedef capd::MpIVector MyVector;
*/
// File for binary output data
extern ofstream outFile;
extern ofstream interFile;

extern char plikZeros[30];
extern char plikInterData[30];
extern char plikZerosTested[30];

extern long methodFaileCounter;
extern long methodZeroes;
extern long collisionCounter;

extern mutex updateCounter_mutex;
extern atomic<long> clusterImprovementCounter;
extern atomic<long> interQuadCounter;
extern MyDouble bias;

extern const MyInterval sqrt2;
extern const MyInterval sqrt3;
extern const int MAX_ITER_NEWTON;
//extern const int NUM_BODIES;
extern int NUM_BODIES;
extern int MAT_DIM;

extern vector<MyDouble> xInit, yInit, massInit;

extern const int usedNewton;
extern const int usedKrawczyk;
extern int methodUsed;
extern int pointInit;
extern int normalizationMethod;
extern bool piszWszystko;
extern bool equalMasses;
extern bool orderIncreasing;
extern bool fixedOrderOfBodies;
extern bool inicjuj;

//void cuteCalculation(MyInterval x, MyInterval y, MyInterval &qx, MyInterval &qy, int rExp);
extern void cuteCalculation(MyInterval x, MyInterval y, MyInterval &qx, MyInterval &qy,
                            int rExp, int bExp);

// 0 -- metoda Newton/Krawczyk bezuzyteczna (macierz osobliwa, wynik z Newtona zawiera warunek poczatkowy)
// 1 -- dokladnie jedno zero
// 2 -- nie ma zer
enum ResultType
{
    uniqueZero,
    methodFailed,
    noZeroInSet,
    collisionFound,
    twoZeros,
    zeroFound // znalazl zero, ale juz je policzyl
};

enum checkZeroResultType
{
    isZeroInside,
    noZeroInside,
    colisionInside
};

enum methodOfNormalization
// bodies numbered from 1, ..., n; last body is computed from the center of mass
{
    equalToZero, // y_{n-1} = 0
    equalToFirst // y_{n-1} = y_1
};

enum initData
{
    noChoice, // generyczny wybor zalezy od parametru R
    leftdown,
    leftup,
    fiveColinear,
    fiveRectangle, // dziala TYLKO dla normalizacji equalToFirst
    fixedFirstAndSecondBodies,
    testForNonSymmetric8Bodies,
    sevenBodies1, // x0\in [-6, -1.5]
    sevenBodies2, // x0\in [-1.5, -1.25]
    sevenBodies3, // x0\in [-1.25, -1.0]
    sevenBodies4, // x0\in [-1.0, -0.75]
    sevenBodies5, // x0\in [-0.75, -0.5]
    sevenBodies6, // x0\in [-0.5, -0.25]
    sevenBodies7, // x0\in [-0.25, 0.0]
    jeden,        // x0\in [-0.75, -0.7]
    proba
};

//extern const int uniqueZero;
//extern const int methodFailed;
//extern const int noZeroInSet;

struct Count
{
    int zeros;
    int maybeZeros;
};

struct Body
{
    MyInterval x, y;
    MyInterval mass;
};

//MyInterval setInterval(MyDouble left, MyDouble right);

////////////////////////////////////////////////////////////////////////////////////////

/** Helpers */

inline MyDouble diam(MyInterval x)
{
    return x.rightBound() - x.leftBound();
}

/*
inline bool outsideZero(MyInterval i)
{
    return i.leftBound() > 0.0 || i.rightBound() < 0.0;
}

inline bool containsZero(MyInterval x)
{
    return (x.leftBound() <= 0.0) && (0.0 <= x.rightBound());
}
*/
//double epsNewton = 0.001;

/** -----------------------------------------------------------
 *  BODIES
 * ------------------------------------------------------------
 **/

class Bodies
{
  public:
    vector<Body> data;
    int N; // liczba cial
    MyInterval massN;

    //    Bodies(const Bodies &source);

    /** Constructing */
    Bodies() {}

    Bodies(const int number)
    {
        N = number;
        Body temp;
        for (int i = 0; i < N - 1; i++)
        {
            data.push_back(temp);
        }
    }

    Bodies(const int number, bool initConstr)
    {

        N = number;

        MyInterval varTemp;
        MyInterval varTempX, varTempY;
        MyInterval justZero(0.0, 0.0);

        massN = 1.0;
        data.resize(N);
        Body b;

        ///*

        MyInterval intervalOverSize = MyInterval(-1e-2, 1e-1);
        massN = MyInterval(0.0);
        MyDouble temp1, temp2;

        for (int i = 0; i < N - 1; i++)
        {
            int id = 2 * i;
            temp1 = xInit.at(id);
            temp2 = xInit.at(id + 1);
            b.x = MyInterval(temp1, temp2) + intervalOverSize;
            temp1 = yInit.at(id);
            temp2 = yInit.at(id + 1);
            if (i == N - 2)
                b.y = MyInterval(0.0, 0.0);
            else
                b.y = MyInterval(temp1, temp2) + intervalOverSize;
            temp1 = massInit.at(id);
            temp2 = massInit.at(id + 1);
            b.mass = MyInterval(temp1, temp2);
            massN += b.mass;
            data[i] = b;
        }
        massN = 1.0 - massN;
    }

    /** Last object */

    MyInterval xN() const;
    MyInterval yN() const;

    /** Indexing bodies */
    ///*
    inline MyInterval x(int i) const { return (i == N - 1) ? xN() : data[i].x; }
    inline MyInterval y(int i) const { return (i == N - 1) ? yN() : data[i].y; }
    inline MyInterval mass(int i) { return (i == N - 1) ? massN : data[i].mass; }
    //*/

    /*
    // dla recznego ustawienia czwartego ciala
    inline MyInterval x(int i) const { return data[i].x; }
    inline MyInterval y(int i) const { return data[i].y; }
    inline MyInterval mass(int i) { return data[i].mass; }
    */
    void setX(int i, MyInterval val);
    void setY(int i, MyInterval val);

    /** Distance */
    // x_{ij} = x_j - x_i
    inline MyInterval distX(int i, int j)
    {
        return x(j) - x(i);
    }
    // y_{ij} = y_j - y_i
    inline MyInterval distY(int i, int j)
    {
        return y(j) - y(i);
    }

    /** Radius */
    // r_{ij} = \sqrt{x_{ij}^2 + y_{ij}^2}
    inline MyInterval radius(int i, int j) { return sqrt(power(distX(i, j), 2) + power(distY(i, j), 2)); }

    /** Derivatives */

    MyInterval dpx(int i, int j)
    // dpx[i, j] = \frac{1}{r_{ij}^3} - 3\frac{(x_i - x_j)^2}{r_{ij}^5}
    //
    // = \frac{1}{r_{ij}^3}\left(1 - 3\frac{(x_i - x_j)^2}{r_{ij}^2}\right)
    {
        MyInterval dp;
        MyInterval r3 = power(radius(i, j), -3);

        MyInterval qx, qy;
        cuteCalculation(distX(j, i), distY(j, i), qx, qy, 5, 2);
        dp = r3 - 3.0 * qx;

        return dp;
    }

    MyInterval dpy(int i, int j)
    // dpy[i, j] = \frac{1}{r_{ij}^3} - 3\frac{(y_i - y_j)^2}{r_{ij}^5}
    // = \frac{1}{r_{ij}^3}\left(1 - 3\frac{(y_i - y_j)^2}{r_{ij}^2}\right)
    {
        MyInterval dp;
        MyInterval r3 = power(radius(i, j), -3);

        MyInterval qx, qy;
        cuteCalculation(distX(j, i), distY(j, i), qx, qy, 5, 2);
        dp = r3 - 3 * qy;

        return dp;
    }

    MyInterval pp(int i, int j)
    // przy liczeniu /dxdy
    // pp[i, j] = 3\frac{(x_i - x_j)(y_i - y_j)}{r_{ij}^5}
    {

        MyInterval dp;

        // xy/r^5 = x/r^3 * y/r^2
        MyInterval qx3, qx2, qy3, qy2;
        cuteCalculation(distX(j, i), distY(j, i), qx3, qy3, 3, 1);
        cuteCalculation(distX(j, i), distY(j, i), qx2, qy2, 2, 1);
        dp = 3 * qx3 * qy2;

        return dp;
    }

    /** Collision */
    // jezeli r_ij < eps_1 (czyli daleko od zderzenie)
    bool collisionHappened();

    /** Functions */
    bool GX(int i, MyInterval &gXi); // sila grawitacji
    bool GY(int i, MyInterval &gYi);

    bool fX(int i, MyInterval &fXi, MyInterval &gXi); // fXi --- wartosci pola wektorowego
    bool fY(int i, MyInterval &fYi, MyInterval &gYi);
    //MyInterval distX(int i, int j);
    //MyInterval distY(int i, int j);

    /** Martix of derivarives */

    MyInterval DxfX(int i, int k, MyMatrix &dpxM); /* df^x_i/dx_k  */
    MyInterval DyfY(int i, int k, MyMatrix &dpyM); /* df^y_i/dy_k  */
    MyInterval DxfY(int i, int k, MyMatrix &ppM);  /* df_i^x/dy_k = df_i^y/dy_k  */

    /** Utils */

    void printData(bool writeDistFromZero = false);
    void printDataToLog();
    void printDiam();
    //    void writeBinaryData(ostream &outFile);
    void writeBinaryData(ostream &outFile);
    bool readBinaryData(istream &inFile);

    MyDouble maxDiam(int &max_i, int &coord);
    // szukamy srednicy najdluszego przedzialu
    // znaleziony przedzial opisuje cialo max_i-te, coord mowi, czy nalezy dzielic x, czy y

    MyDouble minDiam(int &min_i, int &coord);

    void printResult(std::ostream &out = std::cout, bool silent = false);

    Bodies *calculateMidBodies();

    //Bodies *calculatePointBodies(int rogi[]);
};

/** Headers */
int initParameters();
void fillDFMatrix(Bodies &bodies, MyMatrix &DF);
void newMatrix(MyMatrix &DF, MyMatrix &DFF);
void inverseTest(MyMatrix &dF);

bool intersectEdgeWithLine(int direction, MyInterval coef, MyInterval qx, MyInterval qy, int edge,
                           MyInterval &pointx, MyInterval &pointy);
MyInterval radiusXYn(MyInterval x, MyInterval y, int exp);
void setMinMax(MyInterval x, MyInterval y,
               MyInterval &qMinX, MyInterval &qMaxX, MyInterval &qMinY, MyInterval &qMaxY, int rExp);

void wypiszPasekPostepu(Bodies &bodies);

Count blowUp(Bodies bodies);
Count search(Bodies bodies);

void duplicateElimination(const char *nazwaPliku, bool sym, int &uniqueSol);
#endif
