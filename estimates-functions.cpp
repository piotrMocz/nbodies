#include "estimates-functions.h"
#include "cc-nb.h"

void supremumForAll(Bodies &bodies, MyDouble &supC)
// I(q)
{
    MyInterval sum = 0.0;
    for (int i = 0; i < NUM_BODIES; i++)
        sum += bodies.mass(i) * (power(bodies.x(i), 2) + power(bodies.y(i), 2));

    supC = sum.rightBound();
}

bool infimumForAll(Bodies &bodies, MyDouble &infC)
// U(q)
{
    MyInterval sum, rTemp;

    infC = 0.0;
    for (int i = 0; i < NUM_BODIES; i++)
        for (int j = i + 1; j < NUM_BODIES; j++)
        {
            rTemp = bodies.radius(i, j);
            if (isSingular(rTemp))
                return false;

            sum += (bodies.mass(i) * bodies.mass(j)) / rTemp;
        }
    infC = sum.leftBound();
    return true;
}

bool findCluster(Bodies bodies, int num, MyDouble delta, vector<int> &cluster)
// w tablicy cluster sa indeksy cial z klastra; numeracja cial od zera, czyli 0, 1, ..., NUM_BODIES-1
// funkcja zwraca false jest klaster jest jednoelementowy -- tzn. cialo o numerze num jest izolowane;
// w odleglosci delta nie ma innych cial
{
    bool result = false;
    int j;
    vector<int> tempT;
    int markedBodies[NUM_BODIES];
    // tabica markerow: markedBodies[i] = 1 oznacza, ze cialo i-te nalezy juz do klastra

    // wyzerowanie wektora klastra
    cluster.clear();

    // zainicjowanie tablicy markerow na zero -- klaster jest pusty
    for (int i = 0; i < NUM_BODIES; i++)
        markedBodies[i] = 0;

    tempT.push_back(num);
    cluster.push_back(num);
    markedBodies[num] = 1;

    do
    {
        j = tempT.back();
        tempT.pop_back();
        for (int i = 0; i < NUM_BODIES; i++)
        {
            if (i == j)
                continue;

            if (markedBodies[i] == 1) // jesli cialo i-te nalezy juz do klastra, to go nie dodajemy
                continue;

            if (bodies.radius(i, j).leftBound() < delta)
            {
                cluster.push_back(i);
                tempT.push_back(i);
                markedBodies[i] = 1;
                result = true;
            }
        }
    } while (!tempT.empty());

    return result;
}

MyInterval supMomInertiaCluster(Bodies &bodies, vector<int> &cluster)
// I_{C,Z}(q) = I_{cluster, bodies}(q), gdzie q -- cialo o y_{n-1} = 0
// I_{C,Z}(q) = \sum_{i\in cluster} m_i * q_i^2
{
    MyInterval sum = 0.0;
    int vectorSize = cluster.size();
    int i;

    for (int j = 0; j < vectorSize; j++)
    {
        i = cluster[j];
        sum += bodies.mass(i) * (power(bodies.x(i), 2) + power(bodies.y(i), 2));
    }

    return sum.rightBound();
}

MyInterval infPotEnergyCluster(Bodies &bodies, vector<int> &cluster)
// U_{C,Z}(q) = inf \sum_{i < j\in cluster} \frac{m_i*m_j}{r_{i, j}}
{
    //MyDouble sum;
    MyInterval rTemp;
    int vectorSize = cluster.size();
    int iIndex, jIndex;

    MyInterval infC = 0.0;
    for (int i = 0; i < vectorSize; i++)
    {
        iIndex = cluster[i];
        for (int j = i + 1; j < vectorSize; j++)
        {
            jIndex = cluster[j];
            rTemp = bodies.radius(iIndex, jIndex);

            infC += (bodies.mass(iIndex).leftBound() * bodies.mass(jIndex).leftBound()) / rTemp.rightBound();
        }
    }

    return infC;
}

MyInterval potential(Bodies &bodies) 
// U = \sum_{i < j} \frac{m_i*m_j}{r_{i, j}}
{
    MyInterval rTemp;

    MyInterval sum = 0.0;
    int N = bodies.N;
    for (int i = 0; i < N; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
          rTemp = bodies.radius(i, j);
          if (isSingular(rTemp))
          {
            cout << "KOLIZJA!!!" << endl;
            exit(1);
          }

          sum += (bodies.mass(i) * bodies.mass(j)) / rTemp;
        }
    }

    return sum;
}

MyInterval momentInertia(Bodies &bodies) 
// I = \sum_{i} m_i*|q_i|^2
{
    MyInterval rTemp;

    MyInterval sum = 0.0;
    int N = bodies.N;
    for (int i = 0; i < N; i++)
    {
         sum += bodies.mass(i) * (power(bodies.x(i), 2) + power(bodies.y(i), 2));
    }

    return sum;
}

MyInterval scalarProduct(MyInterval xi, MyInterval yi, MyInterval xj, MyInterval yj)
{
    return xi * xj + yi * yj;
}

void findClusterComplement(vector<int> &cluster, vector<int> &clusterCompl)
// w tablicy clusterCompl sa indeksy cial nie nalezacych do klastra; numeracja cial od zera, czyli 0, 1, ..., NUM_BODIES-1
{
    int j;
    int clusterSize = cluster.size();
    bool iFound = false;

    clusterCompl.clear();

    for (int i = 0; i < NUM_BODIES; i++) // przegladam wszystkie ciala
    {
        iFound = false;
        j = 0;
        do
        {
            if (i == cluster[j]) // cialo i-te nalzey do clustra
                iFound = true;
            j++;
        } while ((j < clusterSize) && !iFound); //

        if (!iFound) // cialo i-te nie nalezy do clustra
            clusterCompl.push_back(i);
    }
}

MyInterval infFCZ(Bodies &bodies, vector<int> &cluster)
// F_{C,Z}(q) = \sum_{i\in cluster, k\not\in cluster} \frac{m_i * m_k}{r_{i, k}^3} (q_i - q_k|q_i)
{
    MyInterval r3Temp, sPTemp, bMass, infTemp;
    vector<int> clusterCompl;
    MyInterval infF = 0.0;

    int vectorSize = cluster.size();
    if (vectorSize != NUM_BODIES)
    // jezeli klaster zawiera wszystkie ciala, to infF = 0.0 i nie liczymy dalej
    {
        int iIndex, jIndex;

        infTemp = 0.0;
        findClusterComplement(cluster, clusterCompl);
        int complementSize = clusterCompl.size();
        for (int i = 0; i < vectorSize; i++)
        {
            iIndex = cluster[i];
            for (int j = 0; j < complementSize; j++)
            {
                jIndex = clusterCompl[j];
                MyInterval qx, qy;
                MyInterval distx = bodies.distX(jIndex, iIndex);
                MyInterval disty = bodies.distY(jIndex, iIndex);
                cuteCalculation(distx, disty, qx, qy, 3, 1);
                sPTemp = bodies.x(iIndex) * qx + bodies.y(iIndex) * qy;
                bMass = bodies.mass(iIndex) * bodies.mass(jIndex);

                infTemp += bMass * sPTemp;
            }
        }
        infF = infTemp.leftBound();
    }

    return infF;
}

bool isInCluster(vector<int> &cluster, int i)
// zwraca true -- jesli cialo o numerze i nalezy do clustra
{
    int s = cluster.size();
    for (int j = 0; j < s; j++)
        if (cluster[j] == i)
            return true;

    return false; // przejrzal wszystkie ciala z klastra i zadne nie bylo i-tym
}