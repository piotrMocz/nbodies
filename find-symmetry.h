void reflectOX(Bodies &bodies, Bodies &mirrorReflection);
bool colinearCC(Bodies &bodies);
void equalDistances(Bodies &bodies, vector<int> distances[]);
bool symmetricOXCC(Bodies &bodies);

bool equalDistClosest(Bodies &bodies, MyInterval &xEqualDist, MyInterval &yEqualDist, int &coordMinDist);
bool findSymByLine(Bodies &bodies);