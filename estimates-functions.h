#include "cc-nb.h"

void supremumForAll(Bodies &bodies, MyDouble &supC); // I(q)
bool infimumForAll(Bodies &bodies, MyDouble &infC);  // U(q)

bool findCluster(Bodies bodies, int num, MyDouble delta, vector<int> &cluster);
void findClusterComplement(vector<int> &cluster, vector<int> &clusterCompl);

MyInterval scalarProduct(MyInterval xi, MyInterval yi, MyInterval xj, MyInterval yj);

MyInterval supMomInertiaCluster(Bodies &bodies, vector<int> &cluster); // I_{C,Z}
MyInterval infPotEnergyCluster(Bodies &bodies, vector<int> &cluster);  // U_{C,Z}(q)
MyInterval infFCZ(Bodies &bodies, vector<int> &cluster);               // F_{C,Z}

MyInterval potential(Bodies &bodies);
MyInterval momentInertia(Bodies &bodies);

bool isInCluster(vector<int> &cluster, int i);