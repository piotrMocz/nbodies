#include "cc-nb.h"
#include "krawczyk-newton.h"
#include "test-and-help-functions.h"
#include "estimates-functions.h"
#include <algorithm>

MyInterval volume(Bodies &bodies)
{
    int N = bodies.N;
    MyInterval vol = MyInterval(1.0);

    for (int i = 0; i < N; i++)
    {
        vol *= diam(bodies.x(i));

        if (i == N - 2)
            continue;

        vol *= diam(bodies.y(i));
    }

    return vol;
}

/** Martix of derivarives */

void fillMatrices(Bodies &bodies, MyMatrix &dpxMatrix, MyMatrix &dpyMatrix, MyMatrix &ppMatrix)
{
    int N = bodies.N;
    for (int i = 0; i < N; i++)
    {
        for (int j = i; j < N; j++)
        {
            if (i == j)
            {
                dpxMatrix[i][i] = 1.0;
                dpyMatrix[i][i] = 1.0;
                ppMatrix[i][i] = 1.0;
            }
            else
            {
                dpxMatrix[i][j] = bodies.dpx(i, j);
                dpxMatrix[j][i] = dpxMatrix[i][j];

                dpyMatrix[i][j] = bodies.dpy(i, j);
                dpyMatrix[j][i] = dpyMatrix[i][j];

                ppMatrix[i][j] = bodies.pp(i, j);
                ppMatrix[j][i] = ppMatrix[i][j];
            }
        }
    }
}

void fillDFMatrix(Bodies &bodies, MyMatrix &DF)
{

    // macierze
    // dpxM[i, j] -- \frac{\partial}{\partial x_i}(\frac{\partial}{\partial x_j} (\frac{1}{r_{ij}}))

    MyMatrix dpxM(NUM_BODIES, NUM_BODIES), dpyM(NUM_BODIES, NUM_BODIES), ppM(NUM_BODIES, NUM_BODIES);
    fillMatrices(bodies, dpxM, dpyM, ppM);

    for (int i = 0; i < MAT_DIM; ++i)
        for (int j = 0; j < MAT_DIM; ++j)
            if (i % 2 == 0 && j % 2 == 0)
            {
                DF[i][j] = bodies.DxfX(i / 2, j / 2, dpxM);
            }
            else if (i % 2 == 1 && j % 2 == 1)
            {
                DF[i][j] = bodies.DyfY((i - 1) / 2, (j - 1) / 2, dpyM);
            }
            else if (i % 2 == 0)
            {
                DF[i][j] = bodies.DxfY(i / 2, (j - 1) / 2, ppM);
            }
            else if (j % 2 == 0)
            {
                DF[i][j] = bodies.DxfY((i - 1) / 2, j / 2, ppM);
            }
    /*
    cout << endl;
    bodies.printData();
    cout << endl;
    bodies.printDiam();
    cout << endl;
    for (int i = 0; i < MAT_DIM; ++i)
    {
        cout << "{";
        for (int j = 0; j < MAT_DIM; ++j)
        {
            cout << DF[i][j].leftBound() << ", ";
        }
        cout << "}" << endl;
    }
    cout << endl;
    for (int i = 0; i < MAT_DIM; ++i)
    {
        cout << "{";
        for (int j = 0; j < MAT_DIM; ++j)
        {
            cout << diam(DF[i][j]) << ", ";
        }
        cout << "}" << endl;
    }
    exit(1);
    */
}

/** Symmetry elimination */

void newMatrix(MyMatrix &DF, MyMatrix &DFF)
{
    switch (normalizationMethod)
    {
    case equalToFirst:
        // y_1 = y_{n-1}
        {
            const int lastColumn = MAT_DIM - 1;
            for (int i = 0; i < lastColumn; i++)
                for (int j = 0; j < lastColumn; j++)
                    if (j == 1)
                        DFF[i][j] = DF[i][j] + DF[i][lastColumn];
                    else
                        DFF[i][j] = DF[i][j];
        }
        break;

    case equalToZero:
        // y_{n-1} = 0
        // macierz DFF to macierz DF z usunietym ostatnim wierszem i ostatnia kolumna
        {
            for (int i = 0; i < MAT_DIM - 1; i++)
                for (int j = 0; j < MAT_DIM - 1; j++)
                    DFF[i][j] = DF[i][j];
        }
        break;

    default:
        break;
    }
}

/////////////////////////////////////////////////////////

MyInterval rijPower3(Bodies &bodies, int i, int j)
{
    return 1 / (bodies.radius(i, j) * bodies.radius(i, j) * bodies.radius(i, j));
}

MyInterval matrixA(Bodies &bodies, int i, int k)
// liczy (i,k)-element macierzy A = fourBodiesA
{

    MyDouble zero = 0.0;
    MyDouble one = 1.0;
    MyInterval temp = (i == k) ? -1.0 * one : zero;

    if (0 <= k && k <= i - 1)
    {
        for (int j = 0; j <= k; j++)
        {
            temp -= bodies.mass(j) * rijPower3(bodies, i, j);
        }
    }

    if (i <= k && k <= NUM_BODIES - 2)
    {
        for (int j = k + 1; j <= NUM_BODIES - 1; j++)
        {
            temp += bodies.mass(j) * rijPower3(bodies, i, j);
        }
    }

    if (0 <= k && k <= i)
    {
        for (int j = 0; j <= k; j++)
        {
            temp += bodies.mass(j) * rijPower3(bodies, i + 1, j);
        }
    }

    if (i + 1 <= k && k <= NUM_BODIES - 2)
    {
        for (int j = k + 1; j <= NUM_BODIES - 1; j++)
        {
            temp -= bodies.mass(j) * rijPower3(bodies, i + 1, j);
        }
    }

    return temp;
}

void fillAMatrix(Bodies &bodies, MyMatrix &fourBodiesA)
{

    for (int i = 0; i < NUM_BODIES - 1; i++)
        for (int k = 0; k < NUM_BODIES - 1; k++)
            fourBodiesA[i][k] = matrixA(bodies, i, k);
}

bool linearlyIndependend(MyVector &colX, MyVector &colY)
{
    // jezeli procedura zwraca true, to wektory NA PEWNO sa niezalezne;
    // wpp --- moga byc

    int sizeOfVect = colX.dimension();

    // dziele X przez Y
    MyInterval lambda;
    MyInterval tempDiv;
    bool nonDivisorFound = false;

    for (int i = 0; i < sizeOfVect; i++)
    {
        if (isSingular(colY[i]))
            continue;

        if (nonDivisorFound) // w Y byla juz jakas niezerowa wartosc
        {
            tempDiv = colX[i] / colY[i];
            if (!myIntersection(lambda, tempDiv, lambda))
                return true; // wektory na pewno sa liniowo niezalezne
        }
        else // pierwsze niezerowa wartosc
        {
            lambda = colX[i] / colY[i];
            nonDivisorFound = true;
        }
    }
    // dziele Y przez X
    nonDivisorFound = false;
    for (int i = 0; i < sizeOfVect; i++)
    {
        if (isSingular(colX[i]))
            continue;

        if (nonDivisorFound) // w X byla juz jakas niezerowa wartosc
        {
            tempDiv = colY[i] / colX[i];
            if (!myIntersection(lambda, tempDiv, lambda))
                return true; // wektory na pewno sa liniowo niezalezne
        }
        else // pierwsze niezerowa wartosc
        {
            lambda = colY[i] / colX[i];
            nonDivisorFound = true;
        }
    }

    return false;
}

bool testKerA(MyMatrix &matrixA)
{
    // testKerA --- odpowiada TRUE, jesli konfiguracja, dla ktorej policzono macierz A, ma szanse byc centralna!
    //
    // procedura ma sens tylko dla DIM = 4 (cztery ciala), czyli wymiar macierzy A jest 3x3
    //
    // Jezeli znajde dwa wiersze, o ktorych na pewno wiem, ze nie sa wspolliniowe
    // (nie sa liniowo zalezne, czyli sa liniowo niezalezne), to rzad macierzy A jest wiekszy niz 1.
    // Jesli rzad > 1, to dim ker A = 3 - rzad A < 2, zatem konfiguracja moze byc jedynie wspolniowa.
    // Sprawdzam, czy dla wszystkich cial, czy wspolrzedna y zawiera zero -- jesli nie, to nie jest
    // to konfiguracja centralna.
    //
    // Komentarz ogolnoswiatopogladowy:
    // Jezeli macierz A jest odwracalna, to na pewno nie ma zera!
    //
    // Jezeli wszystkie wiersze macierzy A sa niezalezne, to macierz jest odwracalna.
    //
    // Jezeli gdziekolwiek w wierszu wystepuje zero, to wynik nie jest pewny!
    //
    // Biore dwa wiersze (kolumny) X, Y i dziele jeden przez drugi, X/Y.
    // Jezeli procedura (linearlyIndependent) odpowie FALSE, to znaczy, ze X, Y na pewno nie sa wspolliniowe --
    // jakies przedzialy powstale z podzialu maja puste przeciecie:
    // X[i]/Y[i] ma puste przeciecie z X[j]/Y[j].
    //

    MyVector X(NUM_BODIES - 1);
    MyVector Y(NUM_BODIES - 1);

    for (int i = 0; i < NUM_BODIES - 2; i++)
    {
        for (int j = i + 1; j < NUM_BODIES - 1; j++)
        {
            X = matrixA[i];
            Y = matrixA[j];
            if (linearlyIndependend(X, Y)) // jezeli procedura linearlyIndependend(X, Y) zwraca true, to wektory NA PEWNO sa niezalezne;
            {
                return true;
            }
        }
    }
    return false;
}

bool interQuad(MyInterval xSqr, MyInterval xOld, MyInterval &x, MyDouble &percent)
// xSqrt = [a,b] contains new estimate of x^2, we assume that b >= 0!
// xOld current estimate of x
// on return x is new estimate of x
// true is returned if better estiamtion of x is obtained: x is a proper subset of xOld
// false if x = xOld
//
// percent xNew/xOld
{

    MyInterval bSqrt = sqrt(xSqr.right());
    MyInterval x1;
    if (xSqr.leftBound() <= 0) // if a < 0
    {
        x1 = MyInterval(-bSqrt.rightBound(), bSqrt.rightBound()); // x1=  [-sqrt(b), sqrt(b)]
    }
    else
    {
        MyInterval aSqrt = sqrt(xSqr.left());
        MyInterval temp = MyInterval(aSqrt.leftBound(), bSqrt.rightBound());
        MyInterval minusTemp = -temp;
        MyInterval intersect, minusIntersect;
        bool bIntersect, bMinusIntersect;
        bIntersect = myIntersection(xOld, temp, intersect);
        bMinusIntersect = myIntersection(xOld, minusTemp, minusIntersect);

        if (!bIntersect && !bMinusIntersect)
        {
            cout << "interQuad function exit!" << endl;
            exit(1); // impossible! tested in the parent function
        }

        if (bIntersect && bMinusIntersect)
        {
            x1 = intervalHull(intersect, minusIntersect);
        }

        if (!bIntersect && bMinusIntersect)
        {
            x1 = minusIntersect;
        }

        if (bIntersect && !bMinusIntersect)
        {
            x1 = intersect;
        }
    }

    if (!myIntersection(xOld, x1, x))
    {
        cout << "interQuad function exit!" << endl;
        exit(1); // impossible! tested in the parent function
    }
    // test if there is improvement
    if ((x.leftBound() > xOld.leftBound()) || (x.rightBound() < xOld.rightBound()))
    {
        //MyDouble oldLenght = diam(xOld);
        percent = diam(x) / diam(xOld);
        return true;
    }
    else
        return false;
}

bool distinguishedCubeUEqI(MyInterval U, int i0, int coord, MyDouble lengthInt, Bodies &bodies)
// check if it is worth to improve I = U test
// true --- if the length of the longest interval is larger then bias and
//          the change of the length of the interval is equal or greater then some %
// false --- otherwise
{
    //    return false;
    int N = bodies.N;
    MyInterval temp;
    MyDouble percent = 0.0; // percent\in [0,1)
    //if (lengthInt < bias * 20)
    //    return false;

    if (coord == 1 && i0 == N - 2) // to nie ma prawda zadzialac!
        exit(1);

    MyInterval reducedI = 0.0;
    for (int j = 0; j < N; j++)
    {
        if (i0 == j)
            continue;

        reducedI += bodies.mass(j) * (power(bodies.x(j), 2) + power(bodies.y(j), 2));
    }
    if (coord == 0)
    {
        MyInterval xISqr = (U - reducedI) / bodies.mass(i0) - power(bodies.y(i0), 2);
        if (xISqr < 0)
            return false;

        MyInterval xiOld = bodies.x(i0);
        MyInterval xiOldSqr = power(xiOld, 2);
        if (!myIntersection(xISqr, xiOldSqr, temp))
            return false;

        if (interQuad(xISqr, xiOld, temp, percent))
        {
            bodies.setX(i0, temp);
            if (percent < 0.5)
                return true;
        }
    }

    if (coord == 1)
    {
        MyInterval yISqr = (U - reducedI) / bodies.mass(i0) - power(bodies.x(i0), 2);
        if (yISqr < 0)
            return false;

        MyInterval yiOld = bodies.y(i0);
        MyInterval yiOldSqr = power(yiOld, 2);
        if (!myIntersection(yISqr, yiOldSqr, temp))
            return false;

        if (interQuad(yISqr, yiOld, temp, percent))
        {
            bodies.setY(i0, temp);
            if (percent < 0.5)
                return true;
        }
    }
    return false;
}

bool checkUEqI(Bodies &bodies)
{

    MyInterval U = potential(bodies);
    MyInterval I = momentInertia(bodies);

    if (emptyIntersection(U, I))
        return false; // nie ma rozwiazania
    ///*
    else
    {
        MyDouble percent = 0.0;
        int N = bodies.N;

        int dist_i = 0;
        int distCoord = 0;
        MyDouble lengthInt = bodies.maxDiam(dist_i, distCoord);

        if (!distinguishedCubeUEqI(U, dist_i, distCoord, lengthInt, bodies))
            // the below improvement is costly, so we make only the change is big enough
            // if the change on the smallest interval is less the 25% we stop here
            return true; // mozlwie rozwiazanie, ale nie ma po co liczyc dalej, bo zysk jest zbyt maly
        interQuadCounter++;

        for (int i = 0; i < N - 1; i++)
        {
            MyInterval reducedI = 0.0;
            U = potential(bodies);
            for (int j = 0; j < N; j++)
            {
                if (i == j)
                    continue;

                reducedI += bodies.mass(j) * (power(bodies.x(j), 2) + power(bodies.y(j), 2));
            }

            MyInterval xISqr = (U - reducedI) / bodies.mass(i) - power(bodies.y(i), 2);
            if (xISqr < 0)
                return false;

            MyInterval xiOld = bodies.x(i);
            MyInterval xiOldSqr = power(xiOld, 2);
            MyInterval temp;
            if (!myIntersection(xISqr, xiOldSqr, temp))
                return false;

            if (interQuad(xISqr, xiOld, temp, percent))
            {
                bodies.setX(i, temp);
            }

            if (i == N - 2)
                continue;

            MyInterval yISqr = (U - reducedI) / bodies.mass(i) - power(bodies.x(i), 2);
            if (yISqr < 0)
                return false;

            MyInterval yiOld = bodies.y(i);
            MyInterval yiOldSqr = power(yiOld, 2);
            if (!myIntersection(yISqr, yiOldSqr, temp))
                return false;

            if (interQuad(yISqr, yiOld, temp, percent))
            {
                bodies.setY(i, temp);
            }
        }
    }
    //*/
    return true; // mozliwe rozwiazanie
}

bool checkAForZero(Bodies &bodies)
// jezeli macierz A jest odwracalna, to w przedziale na pewno nie ma zera
// jezeli nie ma zera zwracam false;
// wpp true --- moze byc zero
{
    MyMatrix fourBodiesA(NUM_BODIES - 1, NUM_BODIES - 1), C(NUM_BODIES - 1, NUM_BODIES - 1);
    fillAMatrix(bodies, fourBodiesA);

    /*-------------------------------------------------------*/

    // sprawdzam, czy bodies moze byc centralna konfiguracja wspolliniowa --- TYLKO DLA CZTERECH CIAL!
    if (NUM_BODIES == 4)
    {
        if (testKerA(fourBodiesA))
        {
            // konfiguracja moze byc wspolliniowa, tylko wowczas, jesli y = 0, czyli kazde cialo na wspolrzednej y musi zwierac zero!
            for (int i = 0; i < NUM_BODIES - 1; i++)
            {
                if (!isSingular(bodies.y(i)))
                    return false; // nie moze byc konfiguracja centralna!
            }
        }
    }

    /*-------------------------------------------------------*/

    try
    {
        C = capd::matrixAlgorithms::gaussInverseMatrix(fourBodiesA);
    } // C = fourBodiesA^{-1}
    catch (std::runtime_error)
    {
        return true; // jest szansa na zero
    }
    //inverseTest(fourBodiesA);
    return false; // na pewno nie ma zera
}

/////////////////////////////////////////////////////////

void testKrawczyk(Bodies bodies)
{
    MyMatrix DF(MAT_DIM, MAT_DIM), DFF(MAT_DIM - 1, MAT_DIM - 1), C(MAT_DIM - 1, MAT_DIM - 1);

    fillDFMatrix(bodies, DF);
    newMatrix(DF, DFF);

    MyMatrix midDF = capd::vectalg::midMatrix(DFF);
    cout << "midDF" << endl;
    cout << midDF << endl
         << endl;

    try
    {
        C = capd::matrixAlgorithms::gaussInverseMatrix(midDF);
    } // C = midDF^{-1}
    catch (std::runtime_error)
    {
        cout << "Krawczyk singular matrix\n\n";
        return;
    }
    cout << "C " << endl;
    cout << C << endl;

    cout << endl;
    cout << "C*midDF" << endl;
    cout << C * midDF << endl;
}

/************************************************************************************/
/************************************************************************************/
bool calculateFXiFYi(Bodies bodies, int i, MyInterval &fXi, MyInterval &fYi,
                     MyInterval &gXi, MyInterval &gYi)
{
    if (!bodies.fX(i, fXi, gXi) || !bodies.fY(i, fYi, gYi)) // kolizja miedzy cialem i, a jakims innym
        return false;

    return true;
}

checkZeroResultType checkZero(Bodies &bodies, int i, MyDouble &newWidthRel)
{
    // liczy wartosc funkcji fX(i) i pytama, czy jest zero w srodku
    // zwraca:
    // -- isZeroInside,
    // -- noZeroInside,
    // -- colisionInside

    // newWidthRel -- relative shrinkage of change of volume of q_i = newVolume/oldVolume

    newWidthRel = 1.0;
    MyInterval fXi, fYi, gXi, gYi;
    MyInterval old;

    //bodies.printData();
    //cout << "i = " << i << endl;
    if (calculateFXiFYi(bodies, i, fXi, fYi, gXi, gYi)) // cialo i-te z niczym nie koliduje
    {
        if (!isSingular(fXi) ||
            !isSingular(fYi))
        {
            return noZeroInside;
        }
        else
        {
            if (!isSingular(gXi * bodies.y(i) - gYi * bodies.x(i))) // dla 4-cial ten test nic nie daje
                return noZeroInside;                                // point-force collinearity test fails
            else
            {
                int N = bodies.N;
                if (i != N - 1)
                {
                    MyInterval x = bodies.x(i);
                    old = x;
                    myIntersection(x, gXi, x);
                    newWidthRel = diam(x) / diam(old);
                    bodies.setX(i, x);

                    if (i != N - 2)
                    {
                        MyInterval y = bodies.y(i);
                        old = y;
                        myIntersection(y, gYi, y);
                        newWidthRel *= diam(y) / diam(old);
                        bodies.setY(i, y);
                    }
                }

                return isZeroInside;
            }
        }
    }
    else // jest kolizja!
    {
        return colisionInside;
    }
}

bool checkZeroNumbers(MyInterval X, MyInterval Y)
{
    // zwraca TRUE jesli zera nie ma w srodku ani X ani Y!!!

    return !isSingular(X) ||
           !isSingular(Y);
}

//** Procedury testowe */

void printAllData(Bodies &bodies)
{
    MyInterval fXi, fYi, gXi, gYi;
    MyMatrix A(NUM_BODIES - 1, NUM_BODIES - 1);

    cout << endl;
    cout << "-------------------------------------------------------------------------------------------" << endl;
    ///* wspolrzedne i masy cial
    bodies.printData();
    cout << endl;
    //*/

    ///* srednice przedzilow
    bodies.printDiam();
    cout << endl;
    //*/

    ///* wartosci pola wektorowego
    //cout << "Wartosci pola wektorowego:" << endl;
    //cout << "Wartosc policzona w punkcie ";
    //cout << "liczba cial " << NUM_BODIES << endl;
    for (int i = 0; i < NUM_BODIES; i++)
    {
        calculateFXiFYi(bodies, i, fXi, fYi, gXi, gYi);
        //cout << "Wartosci przedzialowe:" << endl;
        //cout << "fXi(" << i << ") = " << fXi << ";  diam = " << diam(fXi) << endl;
        //cout << "fYi(" << i << ") = " << fYi << ";  diam = " << diam(fYi) << endl;
        //cout << endl;
    }
    //*/

    ///* wartosci pola wektorowego dla srodkow przedzialow
    Bodies *midBodies = bodies.calculateMidBodies();
    cout << endl;
    //cout << "Wartosci pola wektorowego dla MidBodies:" << endl;
    for (int i = 0; i < NUM_BODIES; i++)
    {
        calculateFXiFYi(*midBodies, i, fXi, fYi, gXi, gYi);
        //  cout << "fXi(" << i << ") = " << fXi << ";  diam = " << diam(fXi) << endl;
        //  cout << "fYi(" << i << ") = " << fYi << ";  diam = " << diam(fYi) << endl;
    }
    //*/

    ///* macierz A
    fillAMatrix(bodies, A);
    //*/

    cout << "-------------------------------------------------------------------------------------------" << endl
         << endl;
    delete midBodies;
}

void inverseTest(MyMatrix &dF)
{
    MyMatrix C = capd::vectalg::midMatrix(dF);
    MyMatrix C1;
    MyMatrix dF1;

    try
    {
        C1 = capd::matrixAlgorithms::gaussInverseMatrix(C);
        cout << "C1 = " << C1 << endl;
        dF1 = capd::matrixAlgorithms::gaussInverseMatrix(dF);
        cout << "dF1 = " << dF1 << endl;
        dF1 = dF1 * C1;
        cout << "dF1 = " << dF1 << endl;
        cout << "dF * dF1 " << dF * dF1 << endl;
    }
    catch (...)
    {
        cout << " nie udalo sie odwrocic \n";
    }
}

void testIOFiles(Bodies &bodies)
{
    int sizeOfBodies = sizeof(Bodies);
    ofstream outfile;
    outfile.open("junk.dat", ios::binary | ios::out);
    outfile.write((char *)&bodies, sizeOfBodies); // sizeof can take a type
    outfile.close();

    Bodies testBodies(NUM_BODIES, true);
    ifstream infile;
    infile.open("junk.dat", ios::binary | ios::in);
    infile.read((char *)&testBodies, sizeOfBodies);

    infile.close();

    cout << "Oryginalne dane" << endl;
    bodies.printData();
    cout << "Wczytane dane" << endl;
    testBodies.printData();
    exit(1);
}

void testDane(Bodies &bodies)
{
    MyInterval fXi, fYi, gXi, gYi;

    bodies.printData();

    cout << endl;
    cout << "fX/Y: " << endl;
    for (int i = 0; i < NUM_BODIES - 1; i++)
    {
        cout << i << ": " << flush;
        if (bodies.fX(i, fXi, gXi))
            cout << fXi;
        if (bodies.fY(i, fYi, gYi))
            cout << ",  " << fYi << endl;
    }
    cout << endl;
}

bool distanceTest(Bodies &bodies)
// cialo q_{n-2} jest najdalej i jest na prawo:
// na razie dziala tylko w przypadku y_{n-2} = 0
// zakladamy, ze najbardzej oddalonym od srodka ukladu wspolrzednych cialem jest cialo y_{n-2} = 0
// oraz x_{n-2} > 0
//
// w przypadku rownych mas zadamy, aby cialo q_0 bylo najdalej na lewo
//
{
    int N = bodies.N;
    MyInterval dist;

    if (bodies.x(N - 2) <= 0)
    {
        return false; // q_{N-2} has to be furthest to the right!
    }
    MyInterval xFurthestSqr = power(bodies.x(N - 2), 2); // y(NUM_BODIES - 2) = 0

    for (int i = 0; i < N; i++)
    {
        if (i != N - 2)
        {
            dist = power(bodies.x(i), 2) + power(bodies.y(i), 2);
            if (dist.leftBound() > xFurthestSqr.rightBound())
                return false; // (N-2)-th body has to be furthest
        }
    }

    if (equalMasses)
    // x0 <= xi dla wszystkich pozostalych xi
    // y1 <= yi dla wszystkich yi w przypadku liczby cial > 3!
    {
        // x0 <= xi dla wszystkich pozostalych xi
        MyInterval xq0 = bodies.x(0).leftBound();
        for (int i = 1; i < N; i++)
        {
            if (bodies.x(i) < xq0)
                return false;
        }
        ///*
        // y1 <= yi dla wszystkich yi w przypadku liczby cial > 3!
        if (N > 3)
        {
            MyInterval yq1 = bodies.y(1).leftBound();
            for (int i = 0; i < N; i++)
            {
                if (i == 1)
                    continue;

                if (bodies.y(i) < yq1)
                    return false;
            }
        }
        //*/

        if (N > 4)
        {
            if (orderIncreasing)
            {
                // x2 <= x3 <= ... <= x_{n-3} <= x_{n-1}
                MyInterval x = bodies.x(2).leftBound();
                MyInterval xNext;
                for (int i = 3; i < N; i++)
                {
                    if (i == N - 2)
                        continue;

                    xNext = bodies.x(i);
                    if (xNext < x)
                        return false;

                    x = xNext.leftBound();
                }
            }
            else
            {
                // x2 >= ... >= x_{n-3} >= x_{n-1}
                MyInterval x = bodies.x(N - 1).leftBound();
                MyInterval xNext;
                for (int i = N - 3; i > 1; i--)
                {
                    //if (i == N - 2)
                    //  continue;

                    //if (i == 2)
                    //  continue;

                    xNext = bodies.x(i);
                    if (xNext < x)
                        return false;

                    x = xNext.leftBound();
                }
            }
        }
    }
    return true; // mozliwe istnienie rozwiazania spelniajacego zadane warunki
}

void diagnoseClusterTest(Bodies bodies, vector<int> &cluster, MyInterval UCZ, MyInterval ICZ, MyInterval FCZ)
{
    cout << "Procedurea testowa klastra!" << endl;
    bodies.printData();

    cout << "indeksy cial z klastra: ";
    int vectSize = cluster.size();
    for (int i = 0; i < vectSize; i++)
        cout << cluster[i] << ", ";
    cout << endl;

    vector<int> clusterCompl;
    findClusterComplement(cluster, clusterCompl);
    vectSize = clusterCompl.size();

    cout << "indeksy cial z dopelnienia klastra: ";
    for (int i = 0; i < vectSize; i++)
        cout << clusterCompl[i] << ", ";
    cout << endl;

    cout << "UCZ = " << UCZ << ", ICZ = " << ICZ << ", FCZ = " << FCZ << endl;
    cout << endl;
}

bool clusterTest(Bodies &bodies)
// false -- na pewno nie ma rozwiazania
// true -- mozliwe rozwiazanie
{
    vector<int> cluster;
    MyInterval ICZ, UCZ, FCZ;
    MyDouble delta = eps1 * 50;

    int N = bodies.N;

    for (int i = 0; i < N; i++) // kalster pelny -- wszystkie ciala
        cluster.push_back(i);
    UCZ = infPotEnergyCluster(bodies, cluster);
    ICZ = supMomInertiaCluster(bodies, cluster);
    if (UCZ > ICZ)
    {
        //diagnoseClusterTest(bodies, cluster, UCZ, ICZ, FCZ);
        return false; // na pewno nie ma rozwiazania
    }

    for (int i = 0; i < N - 1; i++)
    {
        if (findCluster(bodies, i, delta, cluster))
        {
            UCZ = infPotEnergyCluster(bodies, cluster);
            ICZ = supMomInertiaCluster(bodies, cluster);
            FCZ = infFCZ(bodies, cluster);
            if (UCZ + FCZ > ICZ)
            {
                //diagnoseClusterTest(bodies, cluster, UCZ, ICZ, FCZ);
                return false; // na pewno nie ma rozwiazania
            }
            if (!zerosInGroup(bodies, cluster))
            {
                return false;
            }
        }
    }

    return true; // mozliwe istnienie rozwiazania
}

bool zerosInGroup(Bodies &bodies, vector<int> &cluster)
{
    int N = bodies.N;
    int s = cluster.size();
    int i;
    MyInterval sumX = 0.0;
    MyInterval sumY = 0.0;
    MyInterval r3;
    MyInterval qx, qy;

    for (int j = 0; j < s; j++)
    {
        i = cluster[j];
        MyInterval tempX = 0.0;
        MyInterval tempY = 0.0;
        for (int k = 0; k < N; k++)
        {
            if (isInCluster(cluster, k))
                continue;

            cuteCalculation(bodies.distX(k, i), bodies.distY(k, i), qx, qy, 3, 1); // distX/|q|^3
            tempX += bodies.mass(i) * bodies.mass(k) * qx;
            tempY += bodies.mass(i) * bodies.mass(k) * qy;
        }

        sumX += tempX;
        sumY += tempY;
    }
    MyInterval sumXTemp = 0.0;
    MyInterval sumYTemp = 0.0;
    for (int j = 0; j < s; j++)
    {
        i = cluster[j];

        sumXTemp += bodies.mass(i) * bodies.x(i);
        sumYTemp += bodies.mass(i) * bodies.y(i);
    }

    if (!isSingular(sumXTemp - sumX) || !isSingular(sumYTemp - sumY))
    {
        return false; // na pewno nie ma rozwiazania!
    }
    ///*
    else
    {
        int N = bodies.N;
        for (int j0 = 0; j0 < s; j0++)
        {

            int i0 = cluster[j0];

            MyInterval sumXTemp = 0.0;
            MyInterval sumYTemp = 0.0;
            for (int j = 0; j < s; j++)
            {
                if (j == j0)
                    continue;

                int i = cluster[j];

                sumXTemp += bodies.mass(i) * bodies.x(i);
                sumYTemp += bodies.mass(i) * bodies.y(i);
            }
            MyInterval xi0Old = bodies.x(i0);
            MyInterval xi0 = (sumX - sumXTemp) / bodies.mass(i0);
            if (!myIntersection(xi0Old, xi0, xi0))
                return false;
            else
            {
                if (i0 != N - 1)
                {
                    bodies.setX(i0, xi0);
                    if ((xi0.leftBound() > xi0Old.leftBound()) || (xi0.rightBound() < xi0Old.rightBound()))
                    {
                        clusterImprovementCounter++;
                    }
                }
            }

            MyInterval yi0Old = bodies.y(i0);
            MyInterval yi0 = (sumY - sumYTemp) / bodies.mass(i0);
            if (!myIntersection(yi0Old, yi0, yi0))
                return false;
            else
            {
                if (i0 != N - 1 && i0 != N - 2)
                {
                    bodies.setY(i0, yi0);
                    if ((yi0.leftBound() > yi0Old.leftBound()) || (yi0.rightBound() < yi0Old.rightBound()))
                    {
                        clusterImprovementCounter++;
                    }
                }
            }
        }
    }
    //*/
    return true;
}

MyInterval aprioriEstimate(int N)
{
    if (N <= 4)
        return MyInterval(N - 1);
    else
    {
        MyInterval two = 2.0;
        MyInterval e1 = MyInterval(1.0) / 3;
        MyInterval e2 = MyInterval(-2.0) / 3;
        MyInterval e3 = MyInterval(2.0) / 3;
        return (power(two, e1) +
                power(two, e2)) *
               power(MyInterval(N - 2), e3);
    }
}

bool checkAprioriBounds(Bodies &bodies)
// FALSE --- apriori bounds are violated
{
    int N = bodies.N;
    MyInterval aE2 = power(aprioriEstimate(N), 2);

    for (int i = 0; i < N; i++)
    {
        if (power(bodies.x(i), 2) + power(bodies.y(i), 2) > aE2)
            return false;
    }
    return true;
}

bool closeBodiesTest(Bodies &bodies)
// false --- two bodies are too close
// r_{i,j} <= \frac{m_i * m_j}{R^2}, where R^2 -- apriori bounds
{
    int N = bodies.N;
    MyInterval aE2 = sqr(aprioriEstimate(N));

    for (int i = 0; i < N; i++)
        for (int j = i + 1; j < N; j++)
        {
            if (bodies.radius(i, j) <= bodies.mass(i) * bodies.mass(j) / aE2)
                return false;
        }

    return true;
}

bool spreadTest(Bodies &bodies)
{
    int N = bodies.N;
    for (int i = 0; i < N; i++)
        for (int j = i + 1; j < N; j++)
        {
            if (bodies.radius(i, j).rightBound() >= 1)
                return true;
        }
    return false;
}
// tablice testujace czestosc wystepowania cial w boksach

bool testPosition(Bodies &bodies, char uklad[])
{
    int R = 1.0;
    int ROZM = 20; // rozmiar tablicy do testowania czestosci wystapien ciala w boksiku
    int N = bodies.N;
    int u[N];
    int tempX, tempY;
    //bool interesting = true;
    for (int k = 0; k < N; k++)
    {
        //interesting = true;
        tempX = int(((bodies.x(k).leftBound() + bodies.x(k).rightBound()) / (4.0 * R) + 0.5) * ROZM);
        tempY = int(((bodies.y(k).leftBound() + bodies.y(k).rightBound()) / (4.0 * R) + 0.5) * ROZM);
        /*
        if (tempX >= ROZM || tempX <= 0)
            interesting = false;
        if (tempY >= ROZM || tempY <= 0)
            interesting = false;

        if (interesting)
            u[k] = tempX * ROZM + tempY;
            */

        if (tempX >= ROZM || tempX < 0)
            return false;
        if (tempY >= ROZM || tempY < 0)
            return false;
        u[k] = tempX * ROZM + tempY;
    }

    //    if (interesting)
    {
        std::sort(u, u + N);

        for (int k = 0; k < N; k++)
        {
            uklad[2 * k] = 'A' + u[k] / ROZM;
            uklad[2 * k + 1] = 'A' + u[k] % ROZM;
        }
        uklad[2 * N] = 0;
    }
    return true;
}

bool compareSolutions(Bodies &sol1, Bodies &sol2, int permutacje[])
{
    return true;
}

bool nextPerm(int p[], int k)
{
    bool found = false;
    int j;
    int i = k - 1;
    do
    {
        if (p[i] > p[i - 1])
            found = true;
        else
            i--;
    } while (i > 0 && !found);

    if (!found && i == 0)
        return false; // nie ma kolejnej permutacji

    // szukamy najmniejszej liczby wiekszej od p[i-1], sposrod p[i], ..., p[N-1]
    for (j = k - 1; j > i - 1; j--)
        if (p[j] > p[i - 1])
            break;

    std::swap(p[i - 1], p[j]);

    for (j = k - 1; i < j; i++, j--)
        std::swap(p[i], p[j]);
    return true;
}

void writePotAndInertia(Bodies &bodies)
{

    MyInterval U = potential(bodies);
    MyInterval I = momentInertia(bodies);
    MyInterval M = 0; // suma mas!

    int N = bodies.N;
    for (int i = 0; i < N; i++)
        M += bodies.mass(i);

    MyInterval scalingInvariant = U * sqrt(I) / power(M, MyInterval(5.0) / 2.0);
    cout << endl;
    cout << "U = " << U << ", I = " << I
         << ", U*(I)^(1/2)/(M)^(5/2) = " << scalingInvariant
         << endl
         << flush;

    if (equalMasses)
    {
        MyInterval UM = scalingInvariant * power(MyInterval(N), MyInterval(5.0) / 2.0);
        cout << "Moeckel's potential = " << UM << endl
             << endl;
    }
}

bool thereIsNoSolution(Bodies &bodies, long licznik[], bool collision, Count &result)
{
    // test 1 ----------------------------------------------------------------------
    if (!checkAprioriBounds(bodies))
    {
        updateCounter_mutex.lock();
        licznik[0]++;
        updateCounter_mutex.unlock();
        //        levelCounter--;
        result.zeros = 0;
        result.maybeZeros = 0;
        //        progressCounter += pow(2.0, height - levelCounter);
        return false; // there is no solution here
    }

    if (!collision)
    {
        // test 2 ----------------------------------------------------------------------
        if (!checkUEqI(bodies))
        {
            //logFile << "A, " << endl
            //        << flush;
            updateCounter_mutex.lock();
            licznik[9]++;
            updateCounter_mutex.unlock();
            //            levelCounter--;
            result.zeros = 0;
            result.maybeZeros = 0;
            //            progressCounter += pow(2.0, height - levelCounter);
            return false;
        }
    }

    //*/
    /*
    if (!closeBodiesTest(bodies))
    {
        //logFile << "close, " << flush;
        updateCounter_mutex.lock();
        licznik[1]++;
        updateCounter_mutex.unlock();
        levelCounter--;
        result.zeros = 0;
        result.maybeZeros = 0;
        progressCounter += pow(2.0, height - levelCounter);
        return result; // there is no solution here
    }
    */
    ///*

    // test 3 ----------------------------------------------------------------------
    if (!clusterTest(bodies))
    {
        //        logFile << "klaster, " << endl
        //              << flush;
        updateCounter_mutex.lock();
        licznik[2]++;
        updateCounter_mutex.unlock();
        //        levelCounter--;
        result.zeros = 0;
        result.maybeZeros = 0;
        //        progressCounter += pow(2.0, height - levelCounter);

        return false; // there is no solution here
    }

    //*/

    if (fixedOrderOfBodies)
    {
        if (normalizationMethod == equalToZero)
        {
            // test 4 ----------------------------------------------------------------------
            if (!distanceTest(bodies))
            // najdalszym cialem jest (n-1)-wsze cialo, ktore ma wsp. y = 0 i  x > 0; czyli
            // forall i: x_i^2 + y_i^2 < x_{n-1}^2
            {
                //logFile << "dist, " << flush;
                updateCounter_mutex.lock();
                licznik[3]++;
                updateCounter_mutex.unlock();
                //            levelCounter--;
                result.zeros = 0;
                result.maybeZeros = 0;
                //            progressCounter += pow(2.0, height - levelCounter);
                return false; // there is no solution here
            }
        }
    }

    /*
    if (!spreadTest(bodies)) // sprawdzamy, czy wszystkie ciala nie sa blizej niz 1
    {
        //logFile << "spread, " << endl
        //        << flush;
        levelCounter--;
        result.zeros = 0;
        result.maybeZeros = 0;
        progressCounter += pow(2.0, height - levelCounter);
        updateCounter_mutex.lock();
        licznik[8]++;
        updateCounter_mutex.unlock();

        //cout << "Za bliskie ciala!" << endl;
        //bodies.printData();
        //cout << uklad << endl;
        //exit(1);

        return result;
    }
*/
    // test 5 ----------------------------------------------------------------------
    // naive interval evaluation of f_i(bodies); for cc it must be that 0 in f_i(bodies)

    int min_i = 0;
    int minCoord = 0;
    bodies.minDiam(min_i, minCoord);

    MyDouble widthRel;

    //first check for the longestInterval
    //if there is no satisfactory improvement here, skip the test
    widthRel = 1.0;
    ///*
    if (checkZero(bodies, min_i, widthRel) == noZeroInside)
    {
        //logFile << "Zero, " << flush;
        updateCounter_mutex.lock();
        licznik[4]++;
        updateCounter_mutex.unlock();
        //            levelCounter--;
        result.zeros = 0;
        result.maybeZeros = 0;
        //            progressCounter += pow(2.0, height - levelCounter);
        return false; // there is no solution here
    }

    if (widthRel <= 1.0)
    {
        int loop = 1;
        //cout << "WOW! ";
        do
        {
            widthRel = 1.0;
            for (int i = 0; i < NUM_BODIES - 1; i++)
            {
                if (loop == 1 && i == min_i)
                {
                    loop++;
                    continue;
                }

                MyDouble newWidthRel = 1.0;
                if (checkZero(bodies, i, newWidthRel) == noZeroInside)
                {
                    //logFile << "Zero, " << flush;
                    updateCounter_mutex.lock();
                    licznik[4]++;
                    updateCounter_mutex.unlock();
                    //            levelCounter--;
                    result.zeros = 0;
                    result.maybeZeros = 0;
                    //            progressCounter += pow(2.0, height - levelCounter);
                    return false; // there is no solution here
                }
                widthRel *= newWidthRel;
            }
        } while (widthRel < 0.1);
    }
    else
    {
        for (int i = 0; i < NUM_BODIES - 1; i++)
        {
            MyDouble newWidthRel = 1.0;
            if (i == min_i)
                continue;

            if (checkZero(bodies, i, newWidthRel) == noZeroInside)
            {
                //logFile << "Zero, " << flush;
                updateCounter_mutex.lock();
                licznik[4]++;
                updateCounter_mutex.unlock();
                //            levelCounter--;
                result.zeros = 0;
                result.maybeZeros = 0;
                //            progressCounter += pow(2.0, height - levelCounter);
                return false; // there is no solution here
            }
        }
    }
    //*/

    /*
    do
    {
        widthRel = 1.0;
        for (int i = 0; i < NUM_BODIES - 1; i++)
        {
            MyDouble newWidthRel = 1.0;
            if (checkZero(bodies, i, newWidthRel) == noZeroInside)
            {
                //logFile << "Zero, " << flush;
        updateCounter_mutex.lock();
                licznik[4]++;
        updateCounter_mutex.unlock();
                //            levelCounter--;
                result.zeros = 0;
                result.maybeZeros = 0;
                //            progressCounter += pow(2.0, height - levelCounter);
                return false; // there is no solution here
            }
            widthRel *= newWidthRel;
        }
    } while (widthRel < 0.2);
    */

    /*
    for (int i = 0; i < NUM_BODIES - 1; i++)
    {
        MyDouble newWidthRel = 1.0;
        if (checkZero(bodies, i, newWidthRel) == noZeroInside)
        {
            //logFile << "Zero, " << flush;
        updateCounter_mutex.lock();
            licznik[4]++;
        updateCounter_mutex.unlock();
            //            levelCounter--;
            result.zeros = 0;
            result.maybeZeros = 0;
            //            progressCounter += pow(2.0, height - levelCounter);
            return false; // there is no solution here
        }
    }
    */
    /*
    if (!collision)
    {
        if (!checkAForZero(bodies))
        {
            //logFile << "A, " << endl
            //        << flush;
            levelCounter--;
            result.zeros = 0;
            result.maybeZeros = 0;
            progressCounter += pow(2.0, height - levelCounter);
            return result;
        }
    }
    */

    return true;
}