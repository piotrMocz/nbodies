#include "cc-nb.h"
#include "krawczyk-newton.h"
#include "test-and-help-functions.h"
#include "solutions-tests.h"

void reflectOX(Bodies &bodies, Bodies &mirrorReflection)
// TODO: zainicjpwac odpowiednio mirrorReflection
{
    MyInterval temp;

    for (int i = 0; i < bodies.N - 1; i++)
    {
        temp = -bodies.y(i);
        mirrorReflection.setY(i, temp);
    }
}

bool colinearCC(Bodies &bodies)
// jezeli wszystkie y_i = 0, to sprawdzam, czy CC jest wspolniowa
{
    for (int i = 0; i < bodies.N; i++)
    {
        if (!isSingular(bodies.y(i)))
            return false; // jakies cialo nie ma y_i = 0
    }

    //ofstream resFile("res-colinear.csv");

    Bodies mirrorReflection(bodies);
    Bodies hullBodies(bodies);

    reflectOX(bodies, mirrorReflection);
    intervalHullBodies(bodies, mirrorReflection, hullBodies);

    Count zeroFound = {0, 0};

    zeroFound = search(hullBodies);
    //resFile.close();

    if (zeroFound.maybeZeros == 0)
    {
        if (zeroFound.zeros == 1)
            return true;
        else
        {
            cout << "licho wie, czy wspolliniowa!" << endl;
            return false; // licho wie
        }
    }
    else
    {
        zeroFound = blowUp(hullBodies);
        if (zeroFound.maybeZeros == 0)
        {
            if (zeroFound.zeros == 1)
                return true;
            else
            {
                cout << "licho wie, czy wspolliniowa!" << endl;
                return false;
            }
        }
    }
    return false;
}

MyInterval distFromZero(MyInterval x, MyInterval y)
// zwraca kwadrat odleglosci od zera
{
    return (power(x, 2) + power(y, 2));
}

void equalDistances(Bodies &bodies, vector<int> distances[])
// w tablicy distances[i] sa numery cial o tej samej odleglosci od 0 co cialo i-te
{
    int N = bodies.N;
    MyInterval x, y, distXYi, distXYj;

    for (int i = 0; i < N; i++) // niepotrzebna jest tablica az do N
    {
        x = bodies.x(i);
        y = bodies.y(i);
        distXYi = distFromZero(x, y);

        for (int j = i + 1; j < N; j++)
        {
            x = bodies.x(j);
            y = bodies.y(j);
            distXYj = distFromZero(x, y);

            if (!emptyIntersection(distXYi, distXYj))
            {
                distances[i].push_back(j);
            }
        }
    }
}

bool reflectOXTwoBodies(Bodies &bodies, int k, int j, Bodies &swappedBodies)
// jezeli ciala sa symetryczne wzgledem OX, to skoro
// maja te sama odleglosc od 0, to zamieniamy ich y-owe wsp. i zwracamy true
//
// jezeli ciala nie sa symetryczne wzgledem OX zwracacy false
{
    MyInterval temp1, temp2;
    MyInterval y1 = bodies.y(k), y2 = bodies.y(j);

    // teraz wiemy, ze k, j sa symetryczne wzgledem OX

    cout << "ciala przed odbiciem: " << bodies.y(k) << " i " << bodies.y(j) << endl;
    temp1 = -bodies.y(k);
    temp2 = -bodies.y(j);
    if (k < bodies.N - 1)
        swappedBodies.setY(k, temp2);
    if (j < bodies.N - 1)
        swappedBodies.setY(j, temp1);
    cout << "ciala po odbiciu: " << swappedBodies.y(k) << " i " << swappedBodies.y(j) << endl;
    return true;
}

bool symmetricOXCC(Bodies &bodies)
// zwraca true, jesli CC jest symetryczna wzgledem odbicia wzgledem osi OX
{
    if (normalizationMethod != equalToZero)
    {
        cout << "Funkcja symmetricOXCC nie obsluguje tej normalizacj!" << endl;
        exit(1);
    }

    int permutacje[NUM_BODIES];
    for (int i = 0; i < NUM_BODIES; i++)
        permutacje[i] = i;
    int N = bodies.N;
    for (int i = 0; i < N; i++)
    {
        if (i == N - 2)
        { // normalizacja equalToZero
            continue;
        }

        if (!isSingular(bodies.y(i))) // nie zawiera zera
        {
            MyInterval myi = -bodies.y(i);
            MyInterval xi = bodies.x(i);
            for (int j = 0; j < N; j++)
            {
                if (i == j)
                    continue;
                MyInterval xj = bodies.x(j);
                MyInterval yj = bodies.y(j);
                if (!emptyIntersection(xi, xj) && !emptyIntersection(myi, yj))
                {
                    permutacje[i] = j;
                    permutacje[j] = i;
                }
            }
        }
    } // for

    cout << "permutation: ";
    for (int i = 0; i < NUM_BODIES; i++)
        cout << permutacje[i] << ", ";
    cout << endl;
    Bodies symBodies(bodies);

    MyInterval xi, xj;
    MyInterval yi, yj;
    for (int i = 0; i < N - 2; i++)
    {
        if (permutacje[i] == i)
        {
            yi = bodies.y(i);
            if (isSingular(yi)) // jesli cialo lezy na osi OX
                symBodies.setY(i, intervalHull(yi, -yi));
            else
            {
                cout << "there is no symmetry with respect to OX" << endl;
                return false; // konfiguracja nie jest symetryczna wzgledem OX, bo yi nie ma pary
            }
        }
        else
        {
            xi = bodies.x(i);
            xj = bodies.x(permutacje[i]);
            yi = bodies.y(i);
            yj = bodies.y(permutacje[i]);

            symBodies.setX(i, intervalHull(xi, xj));
            symBodies.setY(i, intervalHull(yi, -yj));
        }
    }
    //ofstream resFile("res-symOX.csv");
    Count zeroFound = {0, 0};

    zeroFound = search(symBodies);
    //resFile.close();

    if (zeroFound.maybeZeros == 0)
    {
        if (zeroFound.zeros == 1)
            return true;
        else
        {
            cout << "licho wie przed blowUp!" << endl;
            return false; // licho wie
        }
    }
    else
    {
        zeroFound = blowUp(symBodies);
        if (zeroFound.maybeZeros == 0)
        {
            if (zeroFound.zeros == 1)
                return true;
            else
            {
                cout << "licho wie!" << endl;
                return false;
            }
        }
    }
    return false;
}

bool equalDistClosest(Bodies &bodies, MyInterval &xEqualDist, MyInterval &yEqualDist, int &coordMinDist)
// znajduje cialo o tej samej odleglosci od zera co przedostanie  (y_{N-2} = 0.0),
// ale najblizsze temu cialu
// zwraca true, jesli cialo o tej samej odleglosci od zera istnieje
{
    int N = bodies.N;
    bool found = false;
    MyInterval minDist;
    MyInterval dist;
    coordMinDist = 0;

    MyInterval xOneBeforeLast = bodies.x(N - 2);
    MyInterval yOneBeforeLast = bodies.y(N - 2);
    MyInterval distXYi = distFromZero(xOneBeforeLast, yOneBeforeLast);
    for (int j = 0; j < N; j++)
    {
        if (j == N - 2)
            continue;

        MyInterval xj = bodies.x(j);
        MyInterval yj = bodies.y(j);
        MyInterval distXYj = distFromZero(xj, yj);

        if (!emptyIntersection(distXYi, distXYj))
        {
            dist = power(bodies.distX(j, N - 2), 2) + power(bodies.distY(j, N - 2), 2);
            if (!found)
            {
                found = true;
                minDist = dist;
                coordMinDist = j;
                xEqualDist = xj;
                yEqualDist = yj;
            }
            else if (dist < minDist)
            {
                minDist = dist;
                //cout << "cialo nr " << j << endl;
                coordMinDist = j;
                xEqualDist = xj;
                yEqualDist = yj;
            }
        }
    }
    if (found)
    {
        //cout << "minimalna odleglosc znaleziona " << minDist << endl;
        return true;
    }
    else
        return false;
}

void imageOfPoint(MyInterval p, MyInterval q, MyInterval a, MyInterval b, MyInterval xk, MyInterval yk,
                  MyInterval &xkbis, MyInterval &ykbis)
{
    // mamy zadana prosta (p,q) + t(a,b), gdzie a^2 + b^2 = 1
    // szukamy odbicia punktu (xk, yk) wzgledem tej prostej
    // wynik (xkbis, ykbis)
    //
    // cialo nie lezy na prostej symetralnej, wiec musi miec blizniaka
    // cialo qk odbite wzgledem prostej (p,q) -- (0,0) ma wspolrzedne (x2, y2)
    MyInterval c = scalarProduct(xk - p, yk - q, a, b); // rzut (x,y) - (p,q) na AB
    xkbis = xk - 2.0 * c * a;
    ykbis = yk - 2.0 * c * b;
}

bool findSymByLine(Bodies &bodies)
{
    int N = bodies.N;
    MyInterval xA, yA;
    int coordMinDist;
    //bool found = false;
    int permutacje[NUM_BODIES];
    for (int i = 0; i < NUM_BODIES; i++)
        permutacje[i] = i;

    // obliczanie parametrow definiujacych prosta symetralna (p,q) + t(a,b)
    MyInterval xB = bodies.x(N - 2);
    MyInterval yB = bodies.y(N - 2);
    if (!equalDistClosest(bodies, xA, yA, coordMinDist))
        return false; // nie ma ciala rownoodleglego od 0 tak jak przedostatnie cialo
    permutacje[coordMinDist] = N - 2;
    permutacje[N - 2] = coordMinDist;

    MyInterval p = (xA + xB) / 2.0;
    MyInterval q = (yA + yB) / 2.0;

    MyInterval a2b2 = sqrt(power((xA - xB), 2) + power(yA - yB, 2));
    MyInterval a = (xB - xA) / a2b2;
    MyInterval b = (yB - yA) / a2b2;
    // KONIEC: obliczanie parametrow definiujacych prosta symetralna (p,q) + t(a,b)

    if (!isSingular(p * a + q * b))
    {
        cout << "Prosta prostopadla nie przechodzi przez 0!" << endl;

        return false;
    }

    MyInterval xkbis, ykbis;
    // Poczatek  - budowania permutacji
    for (int k = 0; k < N - 2; k++)
    {
        if (k == coordMinDist)
            continue;

        MyInterval xk = bodies.x(k);
        MyInterval yk = bodies.y(k);

        imageOfPoint(p, q, a, b, xk, yk, xkbis, ykbis);
        // czy cialo qk zawadza o prosta symetralna -- permutacje[k] = k
        if (!emptyIntersection(xk, xkbis) && !emptyIntersection(yk, ykbis))
            continue;

        for (int l = 0; l < N; l++)
        {
            if (l == k)
                continue; // qk, ql to jest to samo cialo

            // sprawdzam, czy cialo ql moze byc blizniakiem ciala qk,
            // czyli czy cialo qk odbite wzgledem prostej (p,q) -- (0,0) mogloby byc cialem ql
            MyInterval xl = bodies.x(l);
            MyInterval yl = bodies.y(l);

            if (!emptyIntersection(xl, xkbis) && !emptyIntersection(yl, ykbis))
            {
                permutacje[l] = k;
                permutacje[k] = l;
                break;
            }
        }
    }

    //-----------------------------------------------------------------------------------
    cout << "permutation: ";
    for (int i = 0; i < NUM_BODIES; i++)
        cout << permutacje[i] << ", ";
    cout << endl;

    // Koniec  - budowania permutacji

    // poczatek budowania symetrycznego zbioru do sprawdzenia jednoznacznosci
    Bodies symBodies(bodies);

    MyInterval xi, xj;
    MyInterval yi, yj;
    MyInterval xjBis, yjBis;
    for (int i = 0; i < N - 2; i++)
    {
        // biore cialo qi i jego blizniaka qj = qpermutacje[j]
        // poniewaz j moze byc N-1, to nie mozemy wstawiac za qj!
        //---------------------------------
        if (i == coordMinDist)
            continue;

        xi = bodies.x(i);
        yi = bodies.y(i);
        xj = bodies.x(permutacje[i]);
        yj = bodies.y(permutacje[i]);
        // wyliczam jego odbicie wzgledem prostej i ono trafia w xj -- wskazane przez permutacje
        imageOfPoint(p, q, a, b, xj, yj, xjBis, yjBis);

        symBodies.setX(i, intervalHull(xi, xjBis));
        symBodies.setY(i, intervalHull(yi, yjBis));

        //---------------------------------
    }
    // koniec budowania symetrycznego zbioru do sprawdzenia jednoznacznosci
    //cout << "symetryczne bodies" << endl;
    //symBodies.printData();

    //ofstream resFile("res-symOX.csv");
    Count zeroFound = {0, 0};

    //cout << "sprawdzam dla odbicia symetrycznego" << endl;
    //symBodies.printData();
    //cout << endl;
    outFile.open(plikZerosTested, ios::binary | ios::out | ios::trunc);
    outFile.close();
    zeroFound = search(symBodies);
    //resFile.close();

    int uniqueSol = 0;
    if (zeroFound.maybeZeros == 0)
    {
        if (zeroFound.zeros == 1)
            return true;
        else
        {
            cout << "zera w fazie testowania " << zeroFound.zeros << endl;
            bool symmetryTest = false;
            duplicateElimination(plikZerosTested, symmetryTest, uniqueSol);
            if (uniqueSol == 1)
                return true;
            else
            {
                cout << "Licho wie!" << endl;
                return false; // licho wie
            }
        }
    }
    else
    {
        zeroFound = blowUp(symBodies);
        if (zeroFound.maybeZeros == 0)
        {
            if (zeroFound.zeros == 1)
                return true;
            else
            {
                cout << "licho wie dla odbicia symetrycznego!" << endl;
                return false;
            }
        }
    }
    return false;
}