//#ifndef krawnet_h
//#define krawnet_h

#include "cc-nb.h"

MyVector NewtonOperator(MyMatrix invDF, Bodies *midBodies);

ResultType newtonMethod(Bodies copyBodies, Bodies &returnValues, bool &intersec);
// methodFailed -- Newton bezuzyteczny (macierz osobliwa lub wynik z Newtona zawiera warunek poczatkowy)
// uniqueZero -- dokladnie jedno zero
// noZeroInSet -- nie ma zer
//
// intersec -- czy bylo przeciecie

MyVector krawczykOperator(MyMatrix C, MyMatrix DFF, Bodies bodies);

ResultType krawczykMethod(Bodies copyBodies, Bodies &returnValues, bool &intersec);
// methodFailed -- metoda bezuzyteczna (macierz osobliwa lub wynik zawiera warunek poczatkowy)
// uniqueZero -- dokladnie jedno zero
// noZeroInSet -- nie ma zer
//
// intersec -- czy bylo przeciecie

//////////////////////////////////////////////////////////
// Set and bodies functions
//////////////////////////////////////////////////////////

bool subSet(MyInterval &X, MyInterval &Y);

bool cubeSubSet(MyVector Nx, const Bodies &bodies);

bool cubeSuperSet(MyVector Nx, Bodies &bodies);

bool emptyIntersection(MyInterval &X, MyInterval &Y);

bool cubeEmptyIntersection(MyVector Nx, Bodies &bodies);

bool myIntersection(MyInterval &X, MyInterval &Y, MyInterval &result);

MyVector cubeIntersection(MyVector Nx, Bodies &bodies);

void updateBodies(MyVector Nx, Bodies &bodies);

//#endif
