#include "krawczyk-newton.h"
//#include "read-and-test.h"

const int usedNewton = 0;
const int usedKrawczyk = 1;
//int methodUsed = usedKrawczyk;
int methodUsed;
long liczbaZagniezdzen = 0;

// 0 -- metoda Newton/Krawczyk bezuzyteczna (macierz osobliwa, wynik z Newtona zawiera warunek poczatkowy)
// 1 -- dokladnie jedno zero
// 2 -- nie ma zer
// const int uniqueZero = 1;
// const int methodFailed = 0;
// const int noZeroInSet = 2;

//////////////////////////////////////////////////////////
// Newton method
//////////////////////////////////////////////////////////

MyVector NewtonOperator(MyMatrix invDF, Bodies *midBodies)
{
  MyVector Fx0(MAT_DIM - 1);
  MyVector x0(MAT_DIM - 1);
  MyInterval tempGi;

  //i       0 --  1 --  2 --  3 --  4 --  5
  //bodies x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...
  for (int i = 0; i < MAT_DIM - 1; i++)
    if (i % 2 == 0)
    {
      midBodies->fX(i / 2, Fx0[i], tempGi);
      x0[i] = midBodies->x(i / 2);
    }
    else
    {
      midBodies->fY(i / 2, Fx0[i], tempGi);
      x0[i] = midBodies->y(i / 2);
    }

  return x0 - invDF * Fx0;
}

void midPointMatrix(MyMatrix M, int SIZE)
{

  MyInterval temp, left, right;
  cout << "{";
  for (int i = 0; i < SIZE - 1; i++)
  {
    cout << "{";
    for (int j = 0; j < SIZE - 1; j++)
    {
      cout << M[i][j].mid().leftBound() << ", ";
    }
    cout << M[i][SIZE - 1].mid().leftBound();
    cout << "}," << endl;
  }
  cout << "{";
  for (int j = 0; j < SIZE - 1; j++)
  {
    cout << M[SIZE - 1][j].mid().leftBound() << ", ";
  }
  cout << M[SIZE - 1][SIZE - 1].mid().leftBound();
  cout << "}}" << endl;
}

ResultType newtonMethod(Bodies copyBodies, Bodies &returnValues, bool &intersec)
{
  // methodFailed -- Newton bezuzyteczny (macierz osobliwa lub wynik z Newtona zawiera warunek poczatkowy)
  // uniqueZero -- dokladnie jedno zero
  // noZeroInSet -- nie ma zer

  //Bodies copyBodies (bodies);  // kopiowanie obiektu bodies

  //copyBodies.printData();

  MyMatrix DF(MAT_DIM, MAT_DIM), DFF(MAT_DIM - 1, MAT_DIM - 1), invDF(MAT_DIM - 1, MAT_DIM - 1);
  MyVector Nx(MAT_DIM), Y(MAT_DIM);
  int loopC = 0;
  intersec = false;

  //cout << "newtoMethod" << endl;

  bool collision = copyBodies.collisionHappened();

  if (collision)
  {
    cout << "Kolizja! " << endl
         << flush;
    return methodFailed;
  }

  while (loopC < MAX_ITER_NEWTON)
  {
    loopC++;

    fillDFMatrix(copyBodies, DF);

    //  cout << "DF Mid Matrix:" << endl;
    //  midPointMatrix(DF, MAT_DIM);

    newMatrix(DF, DFF);

    //  cout << "DFF Mid Matrix:" << endl;
    //  midPointMatrix(DFF, MAT_DIM - 1);

    MyMatrix midDF = capd::vectalg::midMatrix(DFF);
    MyMatrix invMidDF = capd::matrixAlgorithms::gaussInverseMatrix(midDF); // DF11 = mid^{-1}
    try
    {
      invDF = invMidDF * capd::matrixAlgorithms::gaussInverseMatrix(DFF * invMidDF);
    }
    catch (std::runtime_error)
    {
      //  cout << "Newton singular matrix\n\n";

      return methodFailed;
    }

    Bodies *midBodies = copyBodies.calculateMidBodies();
    Nx = NewtonOperator(invDF, midBodies);
    // UWAGA!!!!
    delete midBodies;

    if (cubeSubSet(Nx, copyBodies))
    {
      returnValues = copyBodies;
      intersec = true;
      return uniqueZero; // there is exaclty one solution
    }
    else if (cubeEmptyIntersection(Nx, copyBodies))
    {
      //  cout << "!!!!!!!!!!!!!! there is no solution !!!!!!!!!!!!!!!!" << endl;

      returnValues = copyBodies;
      return noZeroInSet; // there is no solution
    }
    else if (cubeSuperSet(Nx, copyBodies))
    {
      //  cout << "!!!!!!!!!!!!!! method failed !!!!!!!!!!!!!!!!" << endl;

      returnValues = copyBodies;
      return methodFailed; // obraz Newtona zawiera copyBodies
    }
    else
    {
      Y = cubeIntersection(Nx, copyBodies);
      updateBodies(Y, copyBodies);
      intersec = true;
    }
  } //       metoda_Newtona();

  //  cout << "!!!!!!!!!!!!!! za duzo iteracji !!!!!!!!!!!!!!!!" << endl;

  returnValues = copyBodies;
  return methodFailed;
}

////////////////////////////////////////////////////////////////////////////////////////

MyVector krawczykOperator(MyMatrix C, MyMatrix DFF, Bodies bodies)
{
  MyVector Fx0(MAT_DIM - 1);
  MyVector x0(MAT_DIM - 1);
  MyVector xMinusx0(MAT_DIM - 1);
  MyMatrix CDFF = -C * DFF;
  Bodies *midBodies = bodies.calculateMidBodies();
  MyDouble x = 1.0;

  MyInterval tempGi;

  for (int i = 0; i < MAT_DIM - 1; i++)
    CDFF[i][i] += x;
  // teraz CDFF = I-C*DFF

  //i       0 --  1 --  2 --  3 --  4 --  5
  //bodies x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...
  for (int i = 0; i < MAT_DIM - 1; i++)
    if (i % 2 == 0)
    {
      midBodies->fX(i / 2, Fx0[i], tempGi);
      x0[i] = midBodies->x(i / 2);
      xMinusx0[i] = bodies.x(i / 2) - x0[i];
    }
    else
    {
      midBodies->fY(i / 2, Fx0[i], tempGi);
      x0[i] = midBodies->y(i / 2);
      xMinusx0[i] = bodies.y(i / 2) - x0[i];
    }
  // UWAGA!!!!
  delete midBodies;

  return x0 - C * Fx0 + CDFF * xMinusx0;
}

/** Krawczyk's method */

ResultType krawczykMethod(Bodies copyBodies, Bodies &returnValues, bool &intersec)
{
  // methodFailed -- metoda bezuzyteczna (macierz osobliwa lub wynik zawiera warunek poczatkowy)
  // uniqueZero -- dokladnie jedno zero (szukam przedzialow o srednicy <= accuracy)
  // noZeroInSet -- nie ma zer
  //
  // jezeli Nx \subset copyBodies, to wiadomo, ze istnieje jednoznaczne zero
  // ale chcemy, aby przedzialy byly male (<= accuracy), wiec petla dziala nadal!
  // wersja krawczykMethodOldVersion() przerywa petle, gdy sie upewni, ze w przedziale jest
  // jedno zero --- niezaleznie od rozmiaru przedzialow

  MyMatrix DF(MAT_DIM, MAT_DIM), DFF(MAT_DIM - 1, MAT_DIM - 1), C(MAT_DIM - 1, MAT_DIM - 1);
  MyVector Nx(MAT_DIM), Y(MAT_DIM);
  int accuracyLoop = 0;
  MyDouble accuracy = 1e-30; // nie zadowala nas znalezienie jednoznacznego zera; chcemy miec maly przedzial
  int N = Nx.dimension();
  bool foundSolution = false;
  intersec = false;

  //  Computation of matrix C
  fillDFMatrix(copyBodies, DF);

  newMatrix(DF, DFF);

  DMatrix midDF(MAT_DIM - 1, MAT_DIM - 1);
  midDF = capd::vectalg::convertObject<DMatrix>(DFF);

  try
  {
    DMatrix doubleC(MAT_DIM - 1, MAT_DIM - 1);
    doubleC = capd::matrixAlgorithms::gaussInverseMatrix(midDF);
    C = capd::vectalg::convertObject<MyMatrix>(doubleC);
    try
    {
      capd::matrixAlgorithms::gaussInverseMatrix(C);
    }
    catch (std::runtime_error)
    {
      //  cout << "Krawczyk singular matrix C!\n\n";

      return methodFailed;
    }
  } // C = midDF^{-1}
  catch (std::runtime_error)
  {
    //  cout << "Krawczyk singular matrix\n\n";

    return methodFailed;
  }

  /////////////////////////
  for (int loopC = 0; loopC < MAX_ITER_NEWTON; loopC++)
  {

    Nx = krawczykOperator(C, DFF, copyBodies);
    if (cubeSubSet(Nx, copyBodies))
    {
      foundSolution = true;
      intersec = true;
      accuracyLoop++;
      updateBodies(Nx, copyBodies);
      for (int i = 0; i < N - 1; i++)
      {
        if (diam(Nx[i]) <= accuracy)
        {
          //cout << "Znalezione rozwiazanie osiagnelo dokladnosc " << accuracy << endl;

          returnValues = copyBodies;
          return uniqueZero; // there is exactly one solution
        }
      }
    }
    else if (cubeEmptyIntersection(Nx, copyBodies))
    {
      return noZeroInSet; // there is no solution
    }
    else if (cubeSuperSet(Nx, copyBodies))
    {

      returnValues = copyBodies;
      if (foundSolution)
        return uniqueZero;
      else
        return methodFailed; // obraz Newtona zawiera copyBodies
    }
    else
    {
      Y = cubeIntersection(Nx, copyBodies);
      updateBodies(Y, copyBodies);
      intersec = true;
    }
  } //       metoda_Newtona();

  returnValues = copyBodies;
  if (foundSolution)
    return uniqueZero;
  else
    return methodFailed;
}

//////////////////////////////////////////////////////////
// Set and bodies functions
//////////////////////////////////////////////////////////

bool subSet(MyInterval &X, MyInterval &Y)
{
  if (X.leftBound() < Y.leftBound())
    return false;
  if (X.rightBound() > Y.rightBound())
    return false;

  return true;
}

bool cubeSubSet(MyVector Nx, const Bodies &bodies)
{
  int N = Nx.dimension();
  MyInterval X;

  //    0 --  1 --  2 --  3 --  4 --  5
  //Nx x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...

  // w normalizationMethod = equalToZero,equalToFirst nie spawrdzamy,
  // czy mamy inkluzje na bodies.y[NUM_BODIES-2], bo to jest wyrzucona zmienna

  for (int i = 0; i < N; i++)
  {
    if (i % 2 == 0)
      X = bodies.x(i / 2);
    else
      X = bodies.y(i / 2);

    if (!subSet(Nx[i], X))
      return false;
  }

  return true;
}

bool cubeSuperSet(MyVector Nx, Bodies &bodies)
{
  int N = Nx.dimension();
  MyInterval X;

  //    0 --  1 --  2 --  3 --  4 --  5
  //Nx x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...

  // w normalizationMethod = equalToZero,equalToFirst nie spawrdzamy,
  // czy mamy inkluzje na bodies.y[NUM_BODIES-2], bo to jest wyrzucona zmienna

  for (int i = 0; i < N; i++)
  {
    if (i % 2 == 0)
      X = bodies.x(i / 2);
    else
      X = bodies.y(i / 2);

    if (!subSet(X, Nx[i]))
      return false;
  }

  return true;
}

bool emptyIntersection(MyInterval &X, MyInterval &Y)
{
  if (X.rightBound() < Y.leftBound() || Y.rightBound() < X.leftBound())
    return true;
  return false;
}

bool cubeEmptyIntersection(MyVector Nx, Bodies &bodies)
{
  int N = Nx.dimension();
  MyInterval X;

  //    0 --  1 --  2 --  3 --  4 --  5
  //Nx x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...

  // w normalizationMethod = equalToZero,equalToFirst nie spawrdzamy,
  // czy mamy inkluzje na bodies.y[NUM_BODIES-2], bo to jest wyrzucona zmienna

  for (int i = 0; i < N; i++)
  {
    if (i % 2 == 0)
      X = bodies.x(i / 2);
    else
      X = bodies.y(i / 2);

    if (emptyIntersection(Nx[i], X))
      return true; // empty intersection
  }

  return false; // non-empty intersection
}

MyDouble maksimum(MyDouble x, MyDouble y)
{
  if (x >= y)
    return x;
  else
    return y;
}

MyDouble minimum(MyDouble x, MyDouble y)
{
  if (x <= y)
    return x;
  else
    return y;
}

bool myIntersection(MyInterval &X, MyInterval &Y, MyInterval &result)
{
  if (emptyIntersection(X, Y))
  {
    //cout << "Error! Empty intesection!" << endl;
    return false;
  }
  result = MyInterval(maksimum(X.leftBound(), Y.leftBound()), minimum(X.rightBound(), Y.rightBound()));
  return true;
}

MyVector cubeIntersection(MyVector Nx, Bodies &bodies)
{
  int N = Nx.dimension();
  MyInterval X;
  MyVector intXY(N);

  //    0 --  1 --  2 --  3 --  4 --  5
  //Nx x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...

  // w normalizationMethod = equalToZero,equalToFirst nie spawrdzamy,
  // czy mamy inkluzje na bodies.y[NUM_BODIES-2], bo to jest wyrzucona zmienna

  for (int i = 0; i < N; i++)
  { // i --- position in vector Nx
    if (i % 2 == 0)
      X = bodies.x(i / 2); // x function
    else
      X = bodies.y(i / 2); // y function

    //    intXY[i] = myIntersection(Nx[i], X);
    myIntersection(Nx[i], X, intXY[i]);
  }

  return intXY;
}

void updateBodies(MyVector Nx, Bodies &bodies)
{
  int N = bodies.N;

  //bodies 0          1           2        ...
  //      x0
  //      0 --  1 --  2 --  3 --  4 --  5
  //Nx   x0 -- y0 -- x1 -- y1 -- x2 -- y2 ...
  for (int i = 0; i < N - 2; i++)
  {
    bodies.setX(i, Nx[2 * i]);
    bodies.setY(i, Nx[2 * i + 1]);
  }
  bodies.setX(N - 2, Nx[2 * (N - 2)]);

  bodies.xN();
  bodies.yN();
}

//////////////////////////////////////////////////////////
