#include "cc-nb.h"
#include "estimates-functions.h"

extern MyDouble eps1;

MyInterval volume(Bodies &bodies);

void fillMatrices(Bodies &bodies, MyMatrix &dpxMatrix, MyMatrix &dpyMatrix, MyMatrix &ppMatrix);
void fillDFMatrix(Bodies &bodies, MyMatrix &DF);
void newMatrix(MyMatrix &DF, MyMatrix &DFF);
MyInterval rijPower3(Bodies &bodies, int i, int j);
MyInterval matrixA(Bodies &bodies, int i, int k);
void fillAMatrix(Bodies &bodies, MyMatrix &fourBodiesA);
bool linearlyIndependend(MyVector &colX, MyVector &colY);
bool testKerA(MyMatrix &matrixA);
bool checkUEqI(Bodies &bodies);
bool checkAForZero(Bodies &bodies);
void testKrawczyk(Bodies bodies);

//bool calculateFXiFYi(Bodies bodies, int i, MyInterval &fXi, MyInterval &fYi);
bool calculateFXiFYi(Bodies bodies, int i, MyInterval &fXi, MyInterval &fYi,
                     MyInterval &gXi, MyInterval &gYi);
checkZeroResultType checkZero(Bodies &bodies, int i, MyDouble &newWidthRel);
bool checkZeroNumbers(MyInterval X, MyInterval Y);
/*
bool calculateNumberFXiFYi(Bodies bodies, int i,
                           int directionFirst, int directionSecond, int directionThird, int directionFourth,
                           MyDouble &fNumberXi, MyDouble &fNumberYi);
bool kombinuj(int p, int rogi[], Bodies &bodies, int i,
              MyDouble &fminXi, MyDouble &fmaxXi, MyDouble &fminYi, MyDouble &fmaxYi);
bool testProcedureFXY(Bodies &bodies, int i, MyInterval &fXi, MyInterval &fYi);
*/
void printAllData(Bodies &bodies);
void inverseTest(MyMatrix &dF);

void testDane(Bodies &bodies);
bool distanceTest(Bodies &bodies);
bool clusterTest(Bodies &bodies);
bool zerosInGroup(Bodies &bodies, vector<int> &cluster);

MyInterval aprioriEstimate(int N);
bool checkAprioriBounds(Bodies &bodies);
bool closeBodiesTest(Bodies &bodies);
bool spreadTest(Bodies &bodies);

// tablice testujace czestosc wystepowania cial w boksach

bool testPosition(Bodies &bodies, char uklad[]);

bool nextPerm(int permutacje[], int k);

void writePotAndInertia(Bodies &bodies);

bool thereIsNoSolution(Bodies &bodies, long licznik[], bool collision, Count &result);
